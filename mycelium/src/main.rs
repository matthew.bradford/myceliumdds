extern crate mycelium_lib;

use std::env;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread::{self, JoinHandle};

use mycelium_lib::prelude::*;

fn load_config() -> std::io::Result<Config> {
    let mut path = match env::current_dir() {
        Ok(p) => p,
        Err(_e) => PathBuf::from(""),
    };
    path.push(Config::file_name());

    let config: (Config, bool) = Config::fetch_or_default(&path).expect("Failed to fetch config");
    if config.1 {
        match config.0.save_y(&path) {
            Ok(_) => (),
            Err(e) => panic!("Failed save: {:?}", e),
        }
    }

    Ok(config.0)
}

///
/// # MyceliumDDM
/// Standalone Mycelium db
///
fn main() {
    let config = load_config().expect("Failed to load config");

    let mut handles = vec![];
    let db = Mycelium::init_db(config, true, true);
    let tcp_db = db.clone();
    handles.push(start_tcp_server(tcp_db));

    let udp_db = db.clone();
    handles.push(start_udp_server(udp_db));

    for handle in handles {
        handle.join().unwrap();
    }
}

fn start_tcp_server(db: Arc<Mycelium>) -> JoinHandle<()> {
    thread::spawn(move || start_local(db).expect("Failed to start TCP server"))
}

fn start_udp_server(db: Arc<Mycelium>) -> JoinHandle<()> {
    thread::spawn(move || start_distributed(db).expect("Failed to start UDP server"))
}
