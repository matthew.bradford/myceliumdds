use super::*;
use crate::test::*;
use mycelium_lib::prelude::*;
use std::collections::HashMap;

const DATADIR_BROTH: &'static str = ".stew/integrations_broth";

fn broth_idx_1() {
    let item_count: usize = 10;
    let tag = "broth_idx_1";
    let config = Config::new().with_data_directory(DATADIR_BROTH);
    let mut items: HashMap<[u8; 16], Vec<String>> = HashMap::new();

    {
        // Scope 1 Creation
        let mut x = Index::init(Some(config.clone()))
            .expect("integrations::broth_idx_1() db init() failed.");
        let summary = x.load_tag(Action::Insert(None), tag);
        assert!(summary.errors.len() == 0);

        let mut hits: Vec<String> = vec![];
        (0..item_count).for_each(|_| {
            hits.push(uuid::Uuid::new_v4().to_string());
        });
        for i in 0..item_count {
            let t = Test {
                test: format!("Test item: {}", i),
                test_int: i as i32,
                test_lint: i as i64,
                test_index: i as usize,
                test_float: i as f32,
                test_lfloat: i as f64,
                test_bool: i % 2 == 0,
                test_vec: format!("Test item: {}", i).as_bytes().to_vec(),
                test_arr: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
            };

            let hits2 = hits.to_vec();
            let mut hit_list = vec![];
            hits.iter().for_each(|i| hit_list.push(i.as_str()));

            let id = x
                .add_indexed(
                    tag,
                    bincode::serialize(&t).unwrap().as_slice(),
                    hit_list.as_slice(),
                );
            items.insert(id.items.first().unwrap().1, hits2);
        }
        match x.save_all() {
            Ok(_) => {}
            Err(e) => {
                assert!(false, format!("Error save_all: {:?}", e));
            }
        }
    }

    {
        // Scope 2 Testing
        let mut idx_stew = Index::init(Some(config.clone()))
            .expect("integrations::broth_idx_get() db init() 2 failed.");
        let summary = idx_stew.load_tag(Action::Default, tag);
        assert!(summary.errors.len() == 0);
        let mut failure = false;
        for (k, v) in items {
            for hit in v {
                let res = idx_stew.search(&[], &[hit.as_str()], true);
                let node = res.items.first().unwrap().clone();
                if node.1 != k {
                    failure = true;
                }
            }
        }

        assert!(failure)
    }
}

#[test]
fn index_search() {
    use crate::mycelium_index::prelude::*;
    use mycelium_command::prelude::*;

    // new db
    let idb = Index::init(None).expect("Failed to create db");
    // add some nodes
    let mut_sum = idb
        .add_indexed("tag2", b"Zippy", &["Shepherd", "Husky", "Akita", "Lab"]);
    let boxer_sum = idb
        .add_indexed("tag2", b"Trip", &["Boxer"]);
    let mut2_sum = idb
        .add_indexed("tag2", b"Buddy", &["Boxer", "Lab"]);
    let pointer_sum = idb
        .add_indexed("tag2", b"Sam", &["Pointer"]);

    let dogs = idb.search(&[], &["Boxer", "Lab"], false);
    assert!(dogs.items.len() == 3);
    // case sensitive
    let dog = idb.search(&[], &["pointer"], false);
    assert!(dog.items.len() == 0);
    let dog = idb.search(&[] ,&["Pointer"], false);
    assert!(dog.items.len() == 1);
}

#[test]
fn broth_idx_get() {
    let test_tag = "broth_get";

    let r = Test::new(String::from("Integrations Test 1"), 1);
    let config = Config::new().with_data_directory(DATADIR_BROTH);
    let r_id: [u8; 16];
    {
        let stew = Index::init(Some(config.clone()))
            .expect("integrations::broth_idx_get() db init() failed.");
        let x = bincode::serialize(&r).unwrap();
        let sum = stew.add_indexed(test_tag, x.as_slice(), &["hit"]);
        r_id = sum.items.first().unwrap().1;
        match stew.save_all() {
            Ok(_) => (),
            Err(e) => assert!(
                false,
                format!("integrations::broth_tests save_all error: {:?}", e)
            ),
        }
    }

    {
        let mut stew = Index::init(Some(config.clone()))
            .expect("integrations::broth_idx_get() db init() 2 failed.");
        let summary = stew.load_tag(Action::Default, test_tag);
        assert!(summary.errors.len() == 0);
        let (tag, id, node) = stew.get(test_tag, &[r_id]).items.first().unwrap().clone();
        let _test: Option<Test> = match bincode::deserialize(node.unwrap().get_item().as_slice()) {
            Ok(t) => Some(t),
            Err(_) => None,
        };
    }
}
