extern crate bincode;
extern crate mycelium_command;
extern crate mycelium_index;
extern crate mycelium_lib;
extern crate rayon;
// ignore warning crate used in tests.
#[allow(unused_imports)]
#[macro_use]
extern crate serde;
extern crate serde_json;
extern crate uuid;

fn main() {}

#[cfg(test)]
mod broth_tests;
#[cfg(test)]
mod stew_tests;
mod test;
