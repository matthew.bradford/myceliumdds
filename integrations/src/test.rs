#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Test {
    pub test: String,
    pub test_int: i32,
    pub test_lint: i64,
    pub test_index: usize,
    pub test_float: f32,
    pub test_lfloat: f64,
    pub test_bool: bool,
    pub test_vec: Vec<u8>,
    pub test_arr: [u8; 16],
}

impl Default for Test {
    fn default() -> Self {
        Test {
            test: "Test".to_string(),
            test_int: 0,
            test_lint: 0,
            test_index: 0,
            test_float: 0.1,
            test_lfloat: 0.22,
            test_bool: false,
            test_vec: vec![],
            test_arr: [0; 16],
        }
    }
}

impl Test {
    #[allow(clippy::neg_multiply, clippy::cast_lossless, dead_code)]
    pub fn new<S>(text: S, index: usize) -> Test
    where
        S: Into<String>,
    {
        let mut test = Test::default();
        test.test = text.into();
        test.test_int = (index as i32 * -1) as i32;
        test.test_lint = (index as i32 * -1) as i64;
        test.test_float = index as f32;
        test.test_lfloat = index as f64;
        test.test_index = index;

        test
    }

    pub fn get_index(&self) -> usize {
        self.test_index
    }

    #[allow(dead_code)]
    pub fn serialize(&self) -> String {
        serde_json::to_string(self).expect("Failed to serialize test data.")
    }
}

use std::cmp::Ordering;
impl std::cmp::Ord for Test {
    fn cmp(&self, other: &Self) -> Ordering {
        self.get_index().cmp(&other.get_index())
    }
}

impl Eq for Test {}

impl PartialOrd for Test {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.get_index().cmp(&other.get_index()))
    }
}

impl PartialEq for Test {
    fn eq(&self, other: &Self) -> bool {
        self.get_index() == other.get_index()
    }
}
