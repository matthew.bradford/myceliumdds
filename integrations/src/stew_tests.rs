use super::*;
use std::error::Error;
use std::io::Write;

use crate::test::*;
use mycelium_lib::prelude::*;

const DATADIR_STEW: &'static str = ".mycelium/integrations";

#[derive(Debug, Deserialize, Serialize)]
struct SaveFetchResult {
    item_save_failed: bool,
    item_update_failed: bool,
    tag_count_failed: bool,
    tag_content_failed: bool,
    history_item_failed: bool,
    pub tag_missing: Vec<Test>,
    pub failures: Vec<String>,
    pub info: Vec<String>,
}

impl Default for SaveFetchResult {
    fn default() -> Self {
        SaveFetchResult {
            item_save_failed: false,
            item_update_failed: false,
            tag_count_failed: false,
            tag_content_failed: false,
            history_item_failed: false,
            tag_missing: vec![],
            failures: vec![],
            info: vec![],
        }
    }
}

impl SaveFetchResult {
    pub fn not_pass(&self) -> bool {
        self.item_save_failed
            || self.item_update_failed
            || self.tag_content_failed
            || self.history_item_failed
            || self.tag_count_failed
            || !self.tag_missing.is_empty()
    }

    pub fn fail_save(&mut self) {
        self.item_save_failed = true
    }
    pub fn fail_content(&mut self) {
        self.tag_content_failed = true
    }
    pub fn fail_update(&mut self) {
        self.item_update_failed = true
    }
    pub fn fail_history(&mut self) {
        self.history_item_failed = true
    }
}

fn end_test(_data_dir: &str, tags: Vec<&str>, result: SaveFetchResult) {
    let b_result = result.not_pass();
    for x in tags {
        if b_result {
            let fname = format!("results/{}.txt", x);
            let mut f = std::fs::File::create(fname).expect("Failed to create result file.");
            let r_ser = serde_json::to_string_pretty(&result).expect("Pretty json result faield.");
            match f.write_all(r_ser.as_bytes()) {
                Ok(_) => (),
                Err(e) => assert!(false, format!("file::write_all error: {:?}", e)),
            }
            match f.sync_all() {
                Ok(_) => (),
                Err(e) => assert!(false, format!("file::sync_all error: {:?}", e)),
            }
        }
    }

    if b_result {
        assert!(false, "Test Failed: {:?}", result)
    }

    assert!(true, "Test passed.")
}

#[test]
fn add_save_fetch() {
    let test_tag = "add_save_fetch";
    let mut result = SaveFetchResult::default();
    setup_logging(3).expect("Failed to start logging.");
    result
        .info
        .push(String::from("add_save_fetch set_1 start..."));

    let r = Test::new(String::from("Integrations Test 1"), 1);
    let r_id: [u8; 16];
    let config = Config::new().with_data_directory(DATADIR_STEW);
    {
        let stew = Db::init(Some(config.clone()))
            .expect("integrations::add_save_fetch() db init() failed.");
        let x = bincode::serialize(&r).unwrap();
        r_id = stew
            .add(x.as_slice(), test_tag)
            .items
            .first()
            .unwrap()
            .1;
        if r_id == [0; 16] {
            result.fail_save();
            result
                .failures
                .push(String::from("Failed to save in add_save_fetch() block 1. "));
        }

        match stew.save_all() {
            Ok(_) => (),
            Err(e) => {
                result.failures.push(format!(
                    "Failed to save in add_save_fetch(): {}. ",
                    e.description()
                ));
                result.fail_save();
            }
        }
    }

    {
        let stew =
            Db::init(Some(config)).expect("integrations::add_save_fetch() db init() 1 failed.");
        let summary = stew.load_tag(Action::Default, test_tag);
        assert!(summary.errors.len() == 0);
        let (tag, id, node) = stew.get(test_tag, r_id).items.first().unwrap().clone();

        let _test: Option<Test> = match bincode::deserialize(node.unwrap().get_item().as_slice()) {
            Ok(t) => Some(t),
            Err(e) => {
                println!("{}", e.description());
                result.fail_content();
                None
            }
        };

        if id != r_id {
            result.fail_content();
        }
    }

    end_test(DATADIR_STEW, vec![test_tag], result)
}

#[test]
fn add_save_fetch_tag() {
    let set_size = 5;
    let set_tag = "add_save_fetch_tag 01";
    let mut result = SaveFetchResult::default();

    let config = Config::new().with_data_directory(DATADIR_STEW);
    let mut test_items = vec![];
    // init set 1
    {
        let stew = mycelium_lib::prelude::Db::init(Some(config.clone()))
            .expect("bench::add_save_fetch_tag() db init() failed.");

        for i in 0..set_size {
            let r = Test::new(format!("A nice stick: {}", i), i);
            let x = bincode::serialize(&r).unwrap();
            test_items.push(
                stew.add(x.as_slice(), set_tag)
                    .items
                    .first()
                    .unwrap()
                    .1,
            );
        }

        match stew.save_all() {
            Ok(_) => (),
            Err(e) => {
                result.failures.push(format!("Error: {:?}", e));
                result.fail_save();
            }
        }
    }

    // reload/use set 1
    {
        let stew = mycelium_lib::prelude::Db::init(Some(config.clone()))
            .expect("bench::add_save_fetch_tag() db init() 2 failed.");
        let summary = stew.load_tag(Action::Default, set_tag);
        assert!(summary.errors.len() == 0);
        for id in test_items {
            let test = stew.get(set_tag, id);
            if test.items.len() != 1 {
                result.fail_content();
                break;
            }
        }
    }

    end_test(DATADIR_STEW, vec![set_tag], result)
}

#[test]
fn add_save_update() {
    let test_tag = "add_save_update";
    let mut result = SaveFetchResult::default();
    let update_text = "Update Test Name";

    let item1 = Test::new(format!("A nice update test: {}", 1), 1);
    let mut _item1_id = [0; 16];
    let item2 = Test::new(format!("A nice update test: {}", 2), 2);
    let item2_id;
    let item3 = Test::new(format!("A nice update test: {}", 3), 3);
    let _item3_id;

    let config = Config::new().with_data_directory(DATADIR_STEW);
    // init set 1
    {
        let stew = mycelium_lib::prelude::Db::init(Some(config.clone()))
            .expect("bench::add_save_update() db init() failed.");
        _item1_id = stew
            .add(bincode::serialize(&item1).unwrap().as_slice(), test_tag)
            .items
            .first()
            .unwrap()
            .1;
        item2_id = stew
            .add(bincode::serialize(&item2).unwrap().as_slice(), test_tag)
            .items
            .first()
            .unwrap()
            .1;
        _item3_id = stew
            .add(bincode::serialize(&item3).unwrap().as_slice(), test_tag)
            .items
            .first()
            .unwrap()
            .1;

        stew.save_all().expect("Failed to save db.");
    }

    // reload/use set 1
    {
        let stew = mycelium_lib::prelude::Db::init(Some(config.clone()))
            .expect("bench::add_save_update() db init() 2 failed.");
        let summary = stew.load_tag(Action::Default, test_tag);
        assert!(summary.errors.len() == 0);
        let (tag, id, node) = stew.get(test_tag, item2_id).items.first().unwrap().clone();
        let mut item_2_cpy: Test = bincode::deserialize(node.unwrap().get_item().as_slice()).unwrap();
        item_2_cpy.test = String::from(update_text);
        if item_2_cpy != item2 {
            result.fail_content();
        }
        stew.update_node(
            id,
            bincode::serialize(&item_2_cpy).unwrap().as_slice(),
            test_tag,
        )
        .expect("Failed to update node.");
        stew.save_all().expect("Failed to save db.");
    }

    {
        let stew = mycelium_lib::prelude::Db::init(Some(config.clone()))
            .expect("bench::add_save_update() db init() 3 failed.");
        let summary = stew.load_tag(Action::Default, test_tag);
        assert!(summary.errors.len() == 0);
        let (tag, id, node) = stew.get(test_tag, item2_id).items.first().unwrap().clone();
        let item_2_cpy: Test = bincode::deserialize(node.unwrap().get_item().as_slice()).unwrap();

        if item_2_cpy.test != update_text {
            result.fail_update();
        }
    }

    end_test(DATADIR_STEW, vec![test_tag], result)
}

#[test]
fn fetch_item_history() {
    let test_tag = "fetch_item_history";
    let mut result = SaveFetchResult::default();
    let test_item_count = 3;
    let mut test_ids = vec![];

    // init set 1
    {
        let config = Config::new().with_data_directory(DATADIR_STEW);
        let stew = mycelium_lib::prelude::Db::init(Some(config))
            .expect("bench::fetch_item_history() db init() failed");

        for i in 0..test_item_count {
            let item = Test::new(format!("Original History Test Name {}", i), i);
            let id = stew
                .add(bincode::serialize(&item).unwrap().as_slice(), test_tag)
                .items
                .first()
                .unwrap()
                .1;
            test_ids.push((id, item));
        }

        match stew.save_all() {
            Ok(_) => (),
            Err(e) => {
                result.failures.push(format!(
                    "Failed to save in add_save_fetch(): {}. ",
                    e.description()
                ));
                result.fail_save();
            }
        }
    }

    let item2 = &test_ids[1];
    let update_text = "Updated History Test Text.";
    // reload/use set 1
    {
        let config = Config::new().with_data_directory(DATADIR_STEW);
        let stew = mycelium_lib::prelude::Db::init(Some(config))
            .expect("bench::fetch_item_history() reload set1 db init() failed");
        let summary = stew.load_tag(Action::Default, test_tag);
        assert!(summary.errors.len() == 0);
        let (tag, id, node) = stew.get(test_tag, item2.0).items.first().unwrap().clone();
        let mut item_2_cpy: Test = bincode::deserialize(node.unwrap().get_item().as_slice()).unwrap();
        item_2_cpy.test = String::from(update_text);
        if item_2_cpy != item2.1 {
            result.fail_content();
        }
        stew.update_node(
            id,
            bincode::serialize(&item_2_cpy).unwrap().as_slice(),
            test_tag,
        )
        .expect("Failed to update node.");

        match stew.save_all() {
            Ok(_) => (),
            Err(e) => {
                result.failures.push(format!(
                    "Failed to save in add_save_fetch(): {}. ",
                    e.description()
                ));
                result.fail_save();
            }
        }
    }

    {
        let config = Config::new().with_data_directory(DATADIR_STEW);
        let mut stew = mycelium_lib::prelude::Db::init(Some(config))
            .expect("bench::fetch_item_history() reload test db init() failed");
        let summary = stew.load_tag(Action::Default, test_tag);
        assert!(summary.errors.len() == 0);
        let hist = stew.node_history(test_tag, item2.0, 0);
        if hist.is_none() {
            result.failures.push(format!("History is empty."));
        };

        if hist.is_some() {
            let hist = hist.unwrap();
            for hist in hist.1 {
                if hist.get_revision() == 0 {
                    let historic_original: Test =
                        bincode::deserialize(hist.get_item().as_slice()).unwrap();
                    // item2 is currently the version kept in memory created by test.
                    // first history item and this should match
                    if item2.1.test != historic_original.test {
                        result.fail_history();
                    }
                }
            }
        }
    }

    end_test(DATADIR_STEW, vec![test_tag], result)
}
