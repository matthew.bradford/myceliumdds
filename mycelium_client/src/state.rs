use crate::client::tcp_call_resposne;
use conrod_core::position::*;
use conrod_core::{widget, Borderable, Colorable, Labelable, Positionable, Sizeable, Widget};
use mycelium_lib::Mycelium;
use crate::state::DrawContextMenu::Yes;

#[derive(Debug, Deserialize, Serialize)]
pub enum DrawContextMenu {
    Yes(f64, f64),
    No
}

#[allow(dead_code)]
pub struct MGui {
    gui_logo: conrod_core::image::Id,
    // input state
    txt_input_str: String,
    txt_ip: String,
    txt_port: String,
    txt_content: String,
    pub mouse_x: f64,
    pub mouse_y: f64,
    db: std::sync::Arc<mycelium_lib::Mycelium>,
    // Layout state
    state: MGuiState,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct MGuiState {
    height: f64,
    width: f64,
    fifth_width: f64,
    fifth_height: f64,

    main_menu_bar_height: f64,

    bottom_panel_sub_menu_symbol: &'static str,
    bottom_panel_sub_menu_symbol_pad: f64,
    bottom_panel_minimize: bool,
    bottom_panel_width: f64,
    bottom_window_height: f64,
    bottom_window_content_height: f64,
    bottom_window_height_us: Option<f64>,

    sub_menu_bar_height: f64,
    sub_menu_bar_btn_height: f64,
    sub_menu_bar_btn_width: f64,
    btn_width: f64,
    btn_height: f64,
    pad_min: f64,
    pad_small: f64,
    pad_large: f64,

    panel_left_height: f64,
    panel_left_top_pad: f64,
    panel_left_width: f64,
    panel_left_width_us: Option<f64>,
    panel_left_minimize: bool,
    panel_left_minimize_symbol: &'static str,
    panel_left_minimize_btn_pad: f64,
    panel_left_minimize_symbol_pad: f64,

    panel_right_height: f64,
    panel_right_top_pad: f64,
    panel_right_width: f64,
    panel_right_width_us: Option<f64>,
    panel_right_minimize: bool,
    panel_right_minimize_symbol: &'static str,
    panel_right_minimize_btn_pad: f64,
    panel_right_minimize_symbol_pad: f64,

    panel_content_width: f64,
    panel_content_height: f64,

    draw_context_menu: DrawContextMenu,
}

impl MGuiState {
    /// Size layout frames based on other control sizes
    pub fn calc_bounds(&mut self, width: f64, height: f64) {
        self.width = width;
        self.height = height;
        self.fifth_width = width * 0.2;
        self.fifth_height = height * 0.2;
        self.bottom_panel_width = width - 2. * self.pad_min;
        self.bottom_window_height = self.fifth_height;

        self.set_bottom_panel();
        self.set_left_panel();
        self.set_right_panel();
        self.set_content_panel();
    }

    fn set_content_panel(&mut self) {
        self.panel_content_width =
            self.width - (self.panel_left_width + self.panel_right_width + 4. * self.pad_large);
        self.panel_content_height =
            self.height - (self.main_menu_bar_height + self.bottom_window_height);
    }

    fn set_right_panel(&mut self) {
        // height
        self.panel_right_height =
            self.height - (self.bottom_window_height + self.main_menu_bar_height);
        self.panel_right_top_pad = self.main_menu_bar_height + self.pad_min;
        // width
        self.panel_right_width = if self.panel_right_minimize {
            self.panel_right_minimize_symbol = "+";
            self.panel_right_minimize_symbol_pad = self.pad_small;
            self.panel_right_minimize_btn_pad = self.pad_large;
            self.sub_menu_bar_height
        } else {
            self.panel_right_minimize_symbol = "_";
            self.panel_right_minimize_symbol_pad = self.pad_large;
            self.panel_right_minimize_btn_pad = self.pad_large;
            if self.panel_right_width_us.is_some() {
                self.panel_right_width_us.unwrap()
            } else {
                self.fifth_width
            }
        }
    }

    fn set_left_panel(&mut self) {
        // height
        self.panel_left_height =
            self.height - (self.bottom_window_height + self.main_menu_bar_height);
        self.panel_left_top_pad = self.main_menu_bar_height + self.pad_min;
        // width
        self.panel_left_width = if self.panel_left_minimize {
            self.panel_left_minimize_symbol = "+";
            self.panel_left_minimize_symbol_pad = self.pad_small;
            self.panel_left_minimize_btn_pad = self.pad_large;
            self.sub_menu_bar_height
        } else {
            self.panel_left_minimize_symbol = "_";
            self.panel_left_minimize_symbol_pad = self.pad_large;
            self.panel_left_minimize_btn_pad = self.pad_large;
            if self.panel_left_width_us.is_some() {
                self.panel_left_width_us.unwrap()
            } else {
                self.fifth_width
            }
        }
    }

    fn set_bottom_panel(&mut self) {
        self.bottom_window_height = self.sub_menu_bar_height;
        self.bottom_panel_sub_menu_symbol = if self.bottom_panel_minimize {
            self.bottom_window_content_height = 0.;
            self.bottom_panel_sub_menu_symbol_pad = self.pad_small;
            "+"
        } else {
            if self.bottom_window_height_us.is_some() {
                let btn_wndw_usr = self.bottom_window_height_us.unwrap();
                self.bottom_window_height = btn_wndw_usr;
                self.bottom_window_content_height = btn_wndw_usr - self.sub_menu_bar_height;
            } else {
                self.bottom_window_height = self.fifth_height;
                self.bottom_window_content_height = self.fifth_height - self.sub_menu_bar_height;
            }
            self.bottom_panel_sub_menu_symbol_pad = self.pad_large;
            "_"
        };
    }
}

impl MGuiState {
    /// Eventually these will be saved off and reloaded as preferences
    fn new(width: f64, _height: f64) -> Self {
        MGuiState {
            width: 0.0,
            height: 0.0,
            fifth_width: 0.0,
            fifth_height: 0.0,
            main_menu_bar_height: 35.0,
            panel_left_width: width * 0.2,
            panel_left_width_us: Option::None,
            panel_left_minimize: false,
            panel_left_minimize_symbol: "",
            panel_left_minimize_btn_pad: 0.0,
            panel_left_minimize_symbol_pad: 0.0,
            panel_left_height: 0.0,
            panel_left_top_pad: 0.0,
            panel_right_height: 0.0,
            panel_right_top_pad: 0.0,
            panel_right_width: width * 0.2,
            panel_right_width_us: Option::None,
            panel_right_minimize: false,
            panel_right_minimize_symbol: "",
            panel_right_minimize_btn_pad: 0.0,
            panel_right_minimize_symbol_pad: 0.0,
            panel_content_width: 0.0,
            panel_content_height: 0.0,
            sub_menu_bar_height: 28.0,
            sub_menu_bar_btn_height: 20.,
            sub_menu_bar_btn_width: 20.,
            bottom_panel_minimize: false,
            bottom_panel_width: 0.0,
            bottom_window_height: 0.0,
            bottom_window_content_height: 0.0,
            bottom_window_height_us: Option::None,
            bottom_panel_sub_menu_symbol: "",
            bottom_panel_sub_menu_symbol_pad: 0.0,
            btn_width: 200.,
            btn_height: 40.,
            pad_min: 1.,
            pad_small: 2.,
            pad_large: 5.,
            draw_context_menu: DrawContextMenu::No
        }
    }
}

impl MGui {
    pub fn new(gui_logo: conrod_core::image::Id, width: f64, height: f64) -> Self {
        let config = mycelium_lib::prelude::Config::new();
        MGui {
            gui_logo,
            txt_input_str: String::new(),
            txt_ip: "127.0.0.1".to_string(),
            txt_port: "34120".to_string(),
            txt_content: String::new(),
            mouse_x: 0.0,
            mouse_y: 0.0,
            db: Mycelium::init_db(config, true, true),
            state: MGuiState::new(width, height),
        }
    }

    /// Probably do some formatting of txt and stuff eventually
    /// Prefer use of this over direct access
    pub fn set_content(&mut self, cnt: String) {
        self.txt_content = cnt;
    }

    pub fn handle_mouse_right_click(&mut self) {
        self.state.draw_context_menu = DrawContextMenu::Yes(self.mouse_x, self.mouse_y);
    }
}

// Generate a unique `WidgetId` for each widget.
widget_ids! {
    pub struct Ids {
        // Layout
        canvas_main,
        panel_menu,
        panel_content,
        // bottom panel layout
        panel_bottom,
        panel_bottom_bar,
        panel_bottom_content,
        panel_bottom_btn_collapse,
        // bottom panel content
        txt_bottom_edit,
        btn_bottom_submit,
        btn_bottom_clear,
        // right panel layout
        panel_right,
        panel_right_bar,
        panel_right_content,
        panel_right_btn_collapse,
        // left panel layout
        panel_left,
        panel_left_bar,
        panel_left_content,
        panel_left_btn_collapse,
        // Content tabs
        txt_main,
        // Context menu
        ctx_menu,
    }
}

/// Instantiate a GUI demonstrating every widget available in conrod.
pub fn gui(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    let win_w = ui.win_w;
    let win_h = ui.win_h;
    app.state.calc_bounds(ui.win_w, ui.win_h);

    widget::Canvas::new()
        .w_h(win_w, win_h)
        .set(ids.canvas_main, ui);

    // Layout areas
    panel_right(ui, ids, app);
    panel_left(ui, ids, app);
    panel_content(ui, ids, app);
    bottom_panel(ui, ids, app);
    bottom_panel_content(ui, ids, app);
    context_menu(ui, ids, app);
    menu(ui, ids, app);
}

fn convert_to_screen(win_w: f64, win_h: f64, x: f64, y: f64) -> (f64, f64) {
    (x - win_w/2.,  win_h/2. - y)
}

fn menu(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    widget::BorderedRectangle::new([app.state.width - app.state.pad_min, app.state.main_menu_bar_height])
        .y_place_on(ids.canvas_main, Place::End(Some(app.state.pad_small)))
        .set(ids.panel_menu, ui);
}

fn context_menu(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {

    match &app.state.draw_context_menu {
        Yes(x, y) => {
            let screen_xy = convert_to_screen(app.state.width, app.state.height, *x, *y);
            widget::BorderedRectangle::new([200., 300.])
                .x(screen_xy.0 + 100.)
                .y(screen_xy.1 - 150.)
                .floating(true)
                .set(ids.ctx_menu, ui);
        },
        _ => (),
    }
}

fn panel_content(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    for txt_edit in widget::TextEdit::new(app.txt_content.as_str())
        .top_left_with_margins_on(
            ids.canvas_main,
            app.state.main_menu_bar_height + app.state.pad_large,
            app.state.panel_left_width + app.state.pad_large,
        )
        .scroll_kids_vertically()
        .wh([
            app.state.panel_content_width,
            app.state.panel_content_height,
        ])
        .color(conrod_core::color::RED)
        .floating(true)
        .set(ids.txt_main, ui)
        {
            app.set_content(txt_edit);
        }
}

fn panel_left(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    // left panel
    widget::BorderedRectangle::new([app.state.panel_left_width, app.state.panel_left_height])
        .top_left_with_margins_on(
            ids.canvas_main,
            app.state.panel_left_top_pad,
            app.state.pad_min,
        )
        .border(app.state.pad_min)
        .set(ids.panel_left, ui);

    // right panel menu bar
    widget::BorderedRectangle::new([app.state.panel_left_width, app.state.sub_menu_bar_height])
        .y_place_on(ids.panel_left, Place::End(Some(app.state.pad_min)))
        .set(ids.panel_left_bar, ui);

    // minimize button on content window bar
    for _press in widget::Button::new()
        .w_h(
            app.state.sub_menu_bar_btn_width,
            app.state.sub_menu_bar_btn_height,
        )
        .label_y(Relative::Scalar(app.state.panel_left_minimize_symbol_pad))
        .label(app.state.panel_left_minimize_symbol)
        .top_right_with_margin_on(ids.panel_left_bar, app.state.panel_left_minimize_btn_pad)
        .set(ids.panel_left_btn_collapse, ui)
        {
            app.state.panel_left_minimize = !app.state.panel_left_minimize;
        }
}

fn panel_right(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    // right panel
    widget::BorderedRectangle::new([app.state.panel_right_width, app.state.panel_right_height])
        .top_right_with_margins_on(
            ids.canvas_main,
            app.state.panel_right_top_pad,
            app.state.pad_min,
        )
        .border(app.state.pad_min)
        .set(ids.panel_right, ui);
    // right panel menu bar
    widget::BorderedRectangle::new([app.state.panel_right_width, app.state.sub_menu_bar_height])
        .y_place_on(ids.panel_right, Place::End(Some(app.state.pad_min)))
        .set(ids.panel_right_bar, ui);

    // minimize button on content window bar
    for _press in widget::Button::new()
        .w_h(
            app.state.sub_menu_bar_btn_width,
            app.state.sub_menu_bar_btn_height,
        )
        .label_y(Relative::Scalar(app.state.panel_right_minimize_symbol_pad))
        .label(app.state.panel_right_minimize_symbol)
        .top_left_with_margin_on(ids.panel_right_bar, app.state.panel_right_minimize_btn_pad)
        .set(ids.panel_right_btn_collapse, ui)
        {
            app.state.panel_right_minimize = !app.state.panel_right_minimize;
        }
}

fn bottom_panel(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    // bottom panel hosting content, title bar, and button area on right side.
    widget::BorderedRectangle::new([app.state.width, app.state.bottom_window_height])
        .y_place_on(ids.canvas_main, Place::Start(None))
        .set(ids.panel_bottom, ui);
    // content window bar
    widget::BorderedRectangle::new([app.state.width, app.state.sub_menu_bar_height])
        .y_place_on(ids.panel_bottom, Place::End(Some(app.state.pad_min)))
        .set(ids.panel_bottom_bar, ui);
    // minimize button on content window bar
    for _press in widget::Button::new()
        .w_h(
            app.state.sub_menu_bar_btn_width,
            app.state.sub_menu_bar_btn_height,
        )
        .label_y(Relative::Scalar(app.state.bottom_panel_sub_menu_symbol_pad))
        .label(app.state.bottom_panel_sub_menu_symbol.to_string().as_str())
        .top_right_with_margin_on(ids.panel_bottom_bar, app.state.pad_large)
        .set(ids.panel_bottom_btn_collapse, ui)
        {
            app.state.bottom_panel_minimize = !app.state.bottom_panel_minimize;
        }

    // hosts controls in bottom window
    widget::BorderedRectangle::new([
        app.state.bottom_panel_width,
        app.state.bottom_window_content_height,
    ])
        .y_place_on(ids.panel_bottom, Place::Start(Some(app.state.pad_min)))
        .set(ids.panel_bottom_content, ui);
}

fn bottom_panel_content(ui: &mut conrod_core::UiCell, ids: &Ids, app: &mut MGui) {
    if app.state.bottom_panel_minimize {
        return;
    }
    // Text box for user input.
    let b_edit_width =
        app.state.bottom_panel_width - (app.state.btn_width + 5. * app.state.pad_large);
    for txt_update in widget::TextEdit::new(app.txt_input_str.as_str())
        .top_left_with_margins_on(
            ids.panel_bottom_content,
            app.state.pad_large,
            app.state.pad_large,
        )
        .w(b_edit_width)
        .floating(true)
        .color(conrod_core::color::YELLOW)
        .set(ids.txt_bottom_edit, ui)
        {
            app.txt_input_str = txt_update;
        }

    // submit button on bottom right
    for _press in widget::Button::new()
        .label("Submit")
        .top_right_with_margins_on(
            ids.panel_bottom_content,
            app.state.pad_large,
            app.state.pad_large,
        )
        .wh([app.state.btn_width, app.state.btn_height])
        .set(ids.btn_bottom_submit, ui)
        {
            let stmt = format!("msql {}", app.txt_input_str);
            match tcp_call_resposne(app.txt_ip.as_str(), app.txt_port.as_str(), stmt.as_str()) {
                Ok(rsp) => app.txt_content = rsp,
                Err(e) => app.txt_content = format!("{:?}", e),
            };
        }

    // clear button on bottom right
    let button_top_margin = app.state.btn_height + 2. * app.state.pad_large;
    for _press in widget::Button::new()
        .label("Clear")
        .mid_bottom_of(ids.btn_bottom_submit)
        .top_right_with_margins_on(
            ids.panel_bottom_content,
            button_top_margin,
            app.state.pad_large,
        )
        .wh([app.state.btn_width, app.state.btn_height])
        .set(ids.btn_bottom_clear, ui)
        {
            app.txt_input_str.clear()
        }
}

/// A set of reasonable stylistic defaults that works for the `gui` below.
pub fn theme() -> conrod_core::Theme {
    conrod_core::Theme {
        name: "Mycelium Dark".to_string(),
        padding: Padding::none(),
        x_position: Position::Relative(Relative::Align(Align::Start), None),
        y_position: Position::Relative(Relative::Direction(Direction::Backwards, 20.0), None),
        background_color: conrod_core::color::BLACK,
        shape_color: conrod_core::color::TRANSPARENT,
        border_color: conrod_core::color::LIGHT_YELLOW,
        border_width: 1.0,
        label_color: conrod_core::color::WHITE,
        font_id: None,
        font_size_large: 26,
        font_size_medium: 18,
        font_size_small: 12,
        widget_styling: conrod_core::theme::StyleMap::default(),
        mouse_drag_threshold: 0.0,
        double_click_threshold: std::time::Duration::from_millis(500),
    }
}
