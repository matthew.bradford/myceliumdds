# Db!

Base of hybrid decentralized database/ecs and eventual relational db/ecs.

Layered application this is the base layer. It is responsible for taking entities
and writing them to disk.

### Terminology:
* Item: 3rd party stuff. Serialized as byte[]
* Node: wrapper containing meta for items.
* TagContainer: Container of nodes.
* Tag: A key to a list of nodes.

#### Base Core Layer
* Read/write disk
* Load/unload manage memory

#### Layer 2
* Persistent App 
* Indexing
* Interpreters (Sql?, JS, ..)

#### Layer 3 (Optional. Shouldn't require layer 2.)
* Networking (distributed w/discovery)

#### General consumer flow:
1) Init Stew
2) Load Tag
3) Perform Action
4) Save
5) Vacuum (optional/occasionally)
