use serde::de::DeserializeOwned;
use std::env;
use std::path::PathBuf;

mod logging;
pub use self::logging::setup_logging;
use crate::fetch::Fetchable;

const FILE_NAME: &str = "mycelium_config.ron";

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Config {
    block_count: usize,
    block_size: usize,
    data_dir: Option<PathBuf>,
    pub tcp_port: usize,
    pub udp_port: usize,
}

impl Fetchable for Config {
    fn deserialize_l<T: DeserializeOwned + Default + Fetchable>(
        f_path: &PathBuf,
    ) -> std::io::Result<T> {
        Config::deserialize_ron(f_path)
    }

    fn serialize_l(&self) -> std::io::Result<Vec<u8>>
    where
        Self: serde::Serialize + Fetchable,
    {
        self.serialize_ron()
    }
}

impl Config {
    pub fn new() -> Config {
        Config::default()
    }

    pub fn file_name() -> &'static str {
        FILE_NAME
    }

    pub fn get_block_size(&self) -> usize {
        self.block_size
    }

    pub fn get_max_page_size(&self) -> usize {
        self.block_size * self.block_count
    }

    pub fn get_page_block_count(&self) -> usize {
        self.block_count
    }

    pub fn get_data_dir(&self) -> Option<PathBuf> {
        self.data_dir.clone()
    }

    /// Default size of block. 1024 byte default.
    pub fn with_block_size(mut self, block_size: usize) -> Self {
        self.block_size = block_size;
        self
    }

    /// Number of blocks stored per page. 30 block default.
    pub fn with_block_count(mut self, block_count: usize) -> Self {
        self.block_count = block_count;
        self
    }

    /// Directory of data store. Required. If not set runtime
    /// errors.
    pub fn with_data_directory(mut self, data_dir: &str) -> Self {
        let path = PathBuf::from(data_dir);
        self.data_dir = Some(path);
        self
    }

    pub fn with_data_directory_pathbuf(mut self, data_dir: PathBuf) -> Self {
        self.data_dir = Some(data_dir);
        self
    }
}

/// Defaults based on machine
impl Default for Config {
    fn default() -> Config {
        let dir = match env::current_dir() {
            Ok(mut exe_path) => {
                exe_path.push("data");
                exe_path
            }
            Err(_) => panic!("Failed to get data path from config"),
        };

        Config {
            block_size: 1024,
            block_count: 30,
            tcp_port: 34120,
            udp_port: 34121,
            data_dir: Some(dir),
        }
    }
}
