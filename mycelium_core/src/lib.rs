extern crate bincode;
extern crate chrono;
extern crate dirs;
extern crate fern;
#[macro_use]
extern crate log;
extern crate rayon;
extern crate remove_dir_all;
extern crate ron;
#[macro_use]
extern crate serde;
extern crate uuid;

use std::collections::HashMap;
use std::path::PathBuf;

pub mod config;
pub mod core;
mod ephemeral;
pub mod fetch;
pub mod maintenance;
mod persistence;
pub mod prelude;
mod system;

use crate::{config::Config, ephemeral::Tag, fetch::Fetchable};
use mycelium_command::prelude::*;
use mycelium_experimental::crossbeam_skiplist::map::SkipMap;

///
/// Identifier for nodes which contain items
///
type NodeId = DbId;

///
/// Identifier for page which contains nodes
///
type PageId = DbId;

///
/// Unique Ids are from crate uuid and feature v4: Uuid::new_v4()
///
pub type DbId = [u8; 16];

pub(crate) const RESERVED: &str = "db_sys";

///
/// # Db - Tagged data storage.
/// HashMap <container_name, container>
///
/// * container \*
///
///     pages: HashMap<PageId, Page>
///
///     index: HashMap<NodeId, PageId>
///
/// *Simplified structure for the data store.
///
pub struct Db {
    config: Config,
    primary: SkipMap<String, Tag>,
    working_dir: std::path::PathBuf,
    system: system::System,
}

///
/// # Db
///
/// Initially load only tag you are interested in. Tags lazy
/// load with use.
///
/// Todo: prune infrequently used data from memory.
///
impl Db {
    ///
    /// # Database init - Primary use
    ///
    /// * Parameters
    ///     - config: Defaults to Config::default()
    ///
    /// * Returns
    ///     - Result<Db>: Instance of Db
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use crate::mycelium_core::prelude::*;
    ///
    /// // Builder pattern for config
    /// let config = Config::new()
    ///     .with_data_directory("./data")
    ///     .with_block_count(32) // blocks per page when run persistently
    ///     .with_block_size(1024); // size of blocks on page    ///
    ///
    /// // TODO: If block count/size are adjusted on an existing Db::vacuum()
    /// // should be run to update existing pages.
    ///
    /// match Db::init(Some(config)) {
    ///     Ok(_) => assert!(true),
    ///     _ => assert!(false, "Failed to create db from config")
    /// }
    ///
    /// ```
    ///
    pub fn init(config: Option<Config>) -> std::io::Result<Db> {
        if config.is_some() {
            return Db::with_config(config.unwrap());
        }
        Db::new()
    }

    // Fails early if there are issues reading from disk.
    ///
    /// # new db
    ///
    /// Db::new() will create a folder structure and system
    /// files used by the database or read existing ones. Nothing
    /// else is written to disk without specific calls to do so.
    ///
    /// * Returns
    ///     - Result<Db>: Instance of db with default config
    ///
    /// ```
    /// use crate::mycelium_core::prelude::*;
    ///
    /// match Db::new() {
    ///     Ok(_) => assert!(true),
    ///     _ => assert!(false, "failed to start db core.")
    /// }
    /// ```
    ///
    #[allow(clippy::new_without_default)]
    pub fn new() -> std::io::Result<Db> {
        let config = Config::default();
        let path = PathBuf::from(config.get_data_dir().unwrap());
        let system: crate::system::System =
            crate::system::System::fetch(RESERVED, system::FILE_NAME, &path)
                .expect("Failed to fetch system info.");
        system.save_x(RESERVED, system::FILE_NAME, &path)?;
        Ok(Db {
            config,
            primary: SkipMap::new(),
            working_dir: std::path::PathBuf::new(),
            system,
        })
    }

    ///
    /// # Add item to container.
    /// * Parameters
    ///     - Item: Reference to byte array of item.
    ///     - Tag: Container for item.
    ///
    /// * Returns
    ///     - Result(DbId)
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let db = Db::new().expect("Failed to create db");
    /// // add an item
    /// let id = db.add(b"new item", "tag").items.first().unwrap().1;
    /// // should get a new id back. Id should not be [0;16] as that is
    /// // an empty id from Uuid.
    /// assert!(id != [0 as u8; 16])
    /// ```
    ///
    pub fn add(&self, item: &[u8], tag: &str) -> Summary {
        core::add(self, item, tag)
    }

    // Create an emtpy structure to the same data directory
    // This is used by vacuum to get an empty structure to move all
    // nodes from one to the other.
    fn empty(&self) -> Db {
        Db {
            config: self.config.clone(),
            primary: SkipMap::new(),
            working_dir: self.working_dir.to_path_buf(),
            system: self.system.clone(),
        }
    }

    ///
    /// # Get a node (struct that wraps the serialized item).
    ///
    /// * Parameters
    ///     - tag: name of container containing the item
    ///     - id: [u8;16] identifier for a head node
    ///
    /// * Returns
    ///     - Option<Node>: None if nothing found.
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// //new db
    /// let db = Db::new().expect("Failed to create db.");
    /// //get an item that doesn't exist
    /// let item_none = db.get("no_exist", [0;16]);
    /// //should be none
    /// assert!(item_none.items.len() == 0, "No Results");
    ///
    /// //add an item to tag tag
    /// let id = db.add(b"item bytes", "tag").items.first().unwrap().1;
    /// //get item just added with id
    /// if let Some(node) = db.get("tag", id).items.first().unwrap().2.clone() {
    ///     assert!(b"item bytes".to_vec() == node.get_item());
    /// } else {
    ///     assert!(false, "Failed to find node");
    /// }
    /// //item should have same contents
    /// ```
    ///
    pub fn get(&self, tag: &str, id: DbId) -> Summary {
        if let Some(tag) = self.primary.get(tag) {
            tag.value().get_node(id)
        } else {
            Summary::new().with_error(format!("Error: Failed lock on container: {}", tag))
        }
    }

    ///
    /// # Get reserved system folder
    ///
    pub fn get_reserved_sys(&self) -> PathBuf {
        let mut reserved = self.working_dir.to_path_buf();
        reserved.push(RESERVED);
        reserved
    }

    ///
    /// # Get all items in tag
    ///
    /// * Parameters
    ///     - tag: Tag identifier to fetch items of
    ///
    /// * Returns:
    ///     - Option<Vec<Node>>: list of nodes
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let db = Db::new().expect("Failed to create db");
    /// // get items for non-existing tag
    /// let empty = db.get_tag("emtpy_tag");
    /// // should be empty
    /// assert!(empty.items.len() == 0, "Tag should be empty");
    ///
    /// //add some items to tag dog
    /// db.add(b"shepherd", "dog").items.first().expect("Failed to add item");
    /// db.add(b"pitbull", "dog").items.first().expect("Failed to add item");
    /// //get a list of items in tag dog
    /// let dog_list = db.get_tag("dog");
    /// //should get 2 back
    /// assert!(dog_list.items.len() == 2);
    ///
    /// ```
    ///
    pub fn get_tag(&self, id: &str) -> Summary {
        if let Some(tag) = self.primary.get(id) {
            tag.value().all_container_nodes()
        } else {
            Summary::new().with_error("Error: Container not found")
        }
    }

    ///
    /// # All nodes in tag as hashmap
    ///
    /// * Parameters:
    ///     - tag: container id
    ///
    ///  * Returns:
    ///     - Option<HashMap<DbId,Node>>>
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let db = Db::new().expect("Failed to create db");
    /// // add an item (returns an id we are ignoring)
    /// db.add(b"Some bytes", "tag").items.first().expect("Failed to add item");
    /// // get hashmap
    /// let hm = db.get_tag_hashmap("tag").expect("Failed to get map");
    ///
    /// assert!(hm.len() == 1);
    ///
    /// ```
    pub fn get_tag_hashmap(&self, tag_id: &str) -> Option<HashMap<DbId, Node>> {
        if let Some(tag) = self.primary.get(tag_id) {
            Some(tag.value().all_container_nodes_map())
        } else {
            None
        }
    }

    ///
    /// # Database working directory
    ///
    pub fn get_working_dir(&self) -> PathBuf {
        self.working_dir.to_path_buf()
    }

    pub fn get_system_id(&self) -> DbId {
        self.system.db_id
    }

    ///
    /// # Empty list if no tags
    ///
    pub fn list_tags(&self) -> Vec<String> {
        let mut tags = vec![];
        for tag in self.primary.iter() {
            if tag.key() == RESERVED {
                continue;
            }
            tags.push(tag.key().to_string())
        }
        tags
    }

    ///
    /// # Load tag from disk
    ///
    /// Multiple calls will destroy in memory changes replacing with what is on disk.
    /// load_tag on emtpy tag will result in container being created.
    ///
    /// * Parameters
    ///     - tag: container name
    ///
    /// * Return
    ///     - Result: ()/Err
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// //new db
    /// let db = Db::new().expect("Failed to create new db");
    ///
    /// let sum = db.load_tag(Action::Default, "no_exists");
    ///
    /// ```
    ///
    pub fn load_tag(&self, actn: Action, tag_id: &str) -> Summary {
        let summary = crate::core::validate_tag(self, actn, tag_id);
        if let Some(tag) = self.primary.get(tag_id) {
            let tag = tag.value().load().with_page_item_index();
            self.primary.insert(tag_id.to_string(), tag);
        }

        summary
    }

    ///
    /// # Load Node History
    ///
    /// Nodes are a lazily loaded linked list with the most recent
    /// version as the head. Load the past versions of a node into memory
    /// before retrieving them.
    ///
    /// * Parameters
    ///     - tag: container name containing node
    ///     - node_id: Id for node to load history list for
    ///
    /// * Returns
    ///     - Tuple.0: Head of node list. None if node has been archived
    ///     - Tuple.1: List of node versions
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let mut db = Db::new().expect("Failed to create db");
    ///
    /// // add item
    /// let id = db.add(b"shepherd", "dog").items.first().unwrap().1;
    /// // update node
    /// db.update_node(id, b"German Shepherd", "dog");
    /// // query history
    /// let result = db.node_history("dog", id, 0).expect("Failed to get node history");
    ///
    /// assert!(result.0.is_some() && result.1.len() ==1);
    /// ```
    ///
    pub fn node_history(
        &self,
        tag_id: &str,
        node_id: DbId,
        limit: usize,
    ) -> Option<(Option<Node>, Vec<Node>)> {
        //let mut current_node = None;
        let mut nodes: Vec<Node> = vec![];
        if let Some(tag) = self.primary.get(tag_id) {
            if tag.value().is_loaded() {
                /*
                current_node = Some(
                    tag.value()
                        .get_node(node_id)
                        .items
                        .get(0)
                        .expect("TODO: Refactor")
                        .clone(),
                );*/

                // Check if history is loaded
                let pages = tag.value().history.get_node_pages(node_id);
                tag.value().history.load_page(pages);

                // get items
                let mut node_hist = tag.value().history.get_nodes(node_id, limit);
                nodes.append(&mut node_hist);
            } else {
                //load from memory (if there)
                let current_node = tag
                    .value()
                    .get_node(node_id)
                    .items
                    .get(0)
                    .expect("TODO: Refactor")
                    .clone();

                let node_hist = tag.value().history.get_nodes(node_id, limit);
                return Some((current_node.2.clone(), node_hist));
            }
        }
        Some((None, nodes))
    }

    #[allow(clippy::match_wild_err_arm)]
    fn with_config(config: Config) -> std::io::Result<Db> {
        let path = match config.get_data_dir() {
            Some(_) => PathBuf::from(config.get_data_dir().unwrap()),
            None => match std::env::current_dir() {
                Ok(p) => p,
                Err(_) => panic!("Couldn't get current executable directory."),
            },
        };
        let sys_path = PathBuf::from(config.get_data_dir().unwrap());
        let system: crate::system::System =
            system::System::fetch(RESERVED, system::FILE_NAME, &sys_path)?;
        system.save_x(RESERVED, system::FILE_NAME, &sys_path)?;
        let max_page_size = config.get_max_page_size();
        let primary = match core::load_tag_containers(&path, max_page_size) {
            Ok(primary) => primary,
            Err(e) => {
                error!("Error loading tags: {}", e.to_string());
                return Err(e);
            }
        };

        let db = Db {
            config,
            primary,
            working_dir: path,
            system,
        };

        // Quick test for things like permissions. We try and fail as
        // early possible so there are no surprises if instantiation is
        // successful.
        validate_probable_success(&db);

        Ok(db)
    }

    ///
    /// # Refresh Tag
    ///
    /// Load tag from disk. Will drop any changes currently in memory.
    ///
    /// * Parameters
    ///     - tag: container name
    ///
    /// * Return
    ///     - Result: (), Box<Err>
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    ///
    /// // new db
    /// let db = Db::new().expect("Failed to create db");
    ///
    /// // Loads from disk.
    /// match db.refresh("dog") {
    ///     Ok(_) => assert!(true),
    ///     Err(_) => assert!(false),
    /// }
    ///
    /// ```
    ///
    pub fn refresh(&self, tag_id: &str) -> Result<(), Box<dyn std::error::Error>> {
        let mut clear = false;
        if let Some(tag) = self.primary.get(tag_id) {
            tag.value().clear();
            clear = true;
        }
        if clear {
            self.load_tag(Action::Default, tag_id);
        }

        Ok(())
    }

    ///
    /// # Write all to disk/cache
    ///
    pub fn save_all(&self) -> Result<(), std::io::Error> {
        crate::core::save_all(self)
    }

    ///
    /// # Update Node
    /// On update new node is pushed onto head of list structure and
    /// original is pushed into history.
    ///
    /// * Parameters:
    ///     - node_id: [u8; 16] id of list head node
    ///     - item: &[u8] byte array of item
    ///     - tag: container id of item
    ///
    /// * Returns
    ///     - Result: (), Err.
    ///
    /// ```
    /// use crate::mycelium_core::Db;
    /// use mycelium_command::prelude::*;
    ///
    /// //new db
    /// let db = Db::new().expect("Failed to create db");
    ///
    /// // add item
    /// let id = db.add(b"item bytes", "item").items.first().unwrap().1;
    /// // update item
    /// match db.update_node(id, b"better bytes", "item") {
    ///     Ok(_) => assert!(true),
    ///     Err(_) => assert!(false, "Failed to update")
    /// }
    ///
    /// ```
    pub fn update_node(
        &self,
        node_id: DbId,
        item: &[u8],
        tag_id: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(tag) = self.primary.get(tag_id) {
            let history_node_summary = tag.value().update_node(node_id, item);
            if let Some(node) = history_node_summary.items.first().unwrap().2.clone() {
                tag.value().update_node_history(node)?;
            }
        }
        Ok(())
    }
}

// Panics
// Tests run on nvme need to try on hdd's
fn file_permission_check(path: std::path::PathBuf) -> Result<(), std::io::Error> {
    use std::fs::File;
    use std::io::Write;
    use uuid::Uuid;

    if !path.exists() {
        std::fs::create_dir_all(path.to_path_buf())?;
    }

    let mut test = path.to_path_buf();
    let path = format!("{}/{}", RESERVED, Uuid::new_v4());
    test.push(&path);
    std::fs::create_dir_all(&test)?;
    test.push("file_test.txt");
    let mut f = File::create(&test)?;
    f.write_all(b"Beef, carrot, peas, and onion.")?;
    match f.sync_all() {
        Ok(_) => (),
        Err(e) => {
            error!("Issues writing files to disk in permission check.");
            return Err(e);
        }
    }

    use remove_dir_all::*;
    remove_dir_all(test.parent().unwrap())
}

fn log_report() {
    info!("Info level logging up");
    trace!("trace level logging up");
    debug!("debug level logging up");
    error!("error level logging up");
    warn!("warning level logging up");
}

///
/// # Logging with crates log and fern
/// Call this once before you setup an instance of stew.
///  If setting up multiple instances calling this again will
///  cause a panic.
///
/// * Parameters:
///     - verbosity: 1 low, 2 med, 3 high. Default 3.
///
/// ```
/// use crate::mycelium_core::prelude::*;
///
/// // most verbose
/// setup_logging(1);
///
/// ```
pub fn setup_logging(verbosity: Option<usize>) {
    #[allow(unused_assignments)]
    let mut verbos = 3;
    if verbosity.is_some() {
        verbos = verbosity.unwrap()
    } else {
        verbos = if cfg!(debug_assertions) { 3 } else { 0 };
    }

    match crate::config::setup_logging(verbos) {
        Ok(_) => {
            log_report();
        }
        Err(e) => panic!("Couldn't init logging: {:?}", e),
    }
}

// Add more as needed as a type of startup validation.
fn validate_probable_success(db: &Db) {
    match file_permission_check(db.working_dir.to_path_buf()) {
        Ok(_) => (),
        Err(e) => panic!("File permission check failure: {}", e.to_string()),
    }
}

#[cfg(test)]
mod tests;
