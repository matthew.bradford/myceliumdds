use serde::Serialize;
use uuid::Uuid;

use std::path::PathBuf;

use crate::ephemeral::{create_page, Page};
use crate::persistence::Persistable;
use crate::{NodeId, PageId};
use mycelium_command::node::Node;
use mycelium_experimental::prelude::*;

const FILENAME: &str = "hist.idx";
const HISTORY_TAG: &str = ".hist";

#[derive(Debug, Deserialize, Serialize)]
pub struct History {
    pub(crate) head: SkipMap<NodeId, SkipSet<PageId>>,
    pub(crate) last_inserted_page: SkipSet<Option<PageId>>,
    head_path: std::path::PathBuf,
    max_page_size: usize,
    #[serde(skip)]
    pub(crate) pages: SkipMap<PageId, Page>,
    working_dir: std::path::PathBuf,
}

// Load last_inserted_page into memory to be ready for updates
// Track changes and save on SaveScope
pub(crate) fn lazy_head(path: &PathBuf, max: usize) -> History {
    let mut working_dir = path.to_path_buf();
    working_dir.push(".hist");
    let mut head_path = working_dir.to_path_buf();
    head_path.push(FILENAME);

    let hist = crate::persistence::history::read_head(&head_path);
    if hist.is_some() {
        hist.unwrap()
    } else {
        new(head_path, working_dir, max)
    }
}

pub(crate) fn new(head_path: PathBuf, working_dir: PathBuf, max: usize) -> History {
    History {
        head: SkipMap::new(),
        last_inserted_page: SkipSet::new(),
        head_path,
        max_page_size: max,
        pages: SkipMap::new(),
        working_dir,
    }
}

impl History {
    /// Append a node to the history. This doesn't remove it from primary and can contain
    /// duplicates.
    /// Typical flow -
    /// Update replaces node in primary structure
    /// Update returns original node
    /// Original node is appended to history after removed from primary.
    #[allow(clippy::map_entry)]
    pub(crate) fn append_history(&self, node: Node) -> Result<(), Box<dyn std::error::Error>> {
        let page_id = self.get_append_page(node.get_size());
        if let Some(page) = self.pages.get(&page_id) {
            let hist_page_id = page.value().add(node.clone());
            if let Some(node_page_list_map) = self.head.get(&node.get_id()) {
                if let Some(_) = node_page_list_map.value().get(&hist_page_id) {
                    // exists
                } else {
                    node_page_list_map.value().insert(hist_page_id);
                }
            } else {
                let set = SkipSet::new();
                set.insert(hist_page_id);
                self.head.insert(node.get_id(), set);
            }
        } else {
            panic!("Failed to find page for history")
        }

        Ok(())
    }

    pub(crate) fn clear(&self) {
        self.pages.clear();
        self.head.clear();
    }

    pub(crate) fn get_append_page(&self, node_size: usize) -> PageId {
        // Check size. If page.size() + node.size() < page.max_size add
        if let Some(lip) = self.last_inserted_page.pop_front() {
            if let Some(page_id) = lip.value() {
                if let Some(page_ref) = self.pages.get(page_id) {
                    if page_ref.value().get_size() + node_size < page_ref.value().get_max_size() {
                        return page_ref.value().get_id();
                    }
                }
            }
        }

        // get a new page either the other is full or we have not inserted yet
        let new_page = create_page(HISTORY_TAG, self.max_page_size);
        let new_page_id = new_page.get_id();
        self.pages.insert(new_page_id, new_page);
        return new_page_id;
    }

    pub(crate) fn get_working_directory(&self) -> PathBuf {
        self.working_dir.to_path_buf()
    }

    pub(crate) fn get_node_pages(&self, node_id: NodeId) -> Vec<PageId> {
        let mut list = vec![];
        if let Some(node_page_map) = self.head.get(&node_id) {
            for pageid in node_page_map.value().iter() {
                list.push(*pageid.value());
            }
        }
        list
    }

    pub(crate) fn get_nodes(&self, id: NodeId, limit: usize) -> Vec<Node> {
        let mut nodes: Vec<Node> = vec![];
        for page in self.pages.iter() {
            for (_, node_id, node) in page.value().get_nodes() {
                if node_id == id {
                    if let Some(n) = node {
                        nodes.push(n);
                    }
                }

                if limit > 0 && nodes.len() == limit {
                    break;
                }
            }
        }

        nodes
    }

    pub(crate) fn load_page(&self, page_ids: Vec<PageId>) {
        let page_path_bs = self.working_dir.to_path_buf();
        for id in page_ids {
            let mut page_path = page_path_bs.to_path_buf();
            let lid = Uuid::from_bytes(id);
            page_path.push(lid.to_string());
            if let Some(page) = crate::persistence::load(page_path) {
                self.pages.insert(id, page);
            }
        }
    }
}

impl Persistable for History {
    fn get_id_str(&self) -> &str {
        FILENAME
    }

    fn save(&self, path: &std::path::PathBuf) -> std::io::Result<()> {
        crate::persistence::persist(self, path)
    }
}
