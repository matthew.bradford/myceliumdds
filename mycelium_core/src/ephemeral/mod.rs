use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;

pub(crate) mod history;
mod page;
pub use self::page::{create_page, Page};
use crate::ephemeral::history::History;
use crate::persistence::{load_from_vec, Persistable};
use crate::{NodeId, PageId};
use mycelium_command::prelude::*;
use mycelium_experimental::prelude::*;

use rayon::prelude::*;

#[derive(Clone, Copy, Deserialize, PartialEq, PartialOrd, Serialize)]
pub enum TagStatus {
    New,
    Loaded,
    Cleared,
}

///
/// # Primary container
///
/// * Members
///     - history: Lazy container only loads after requests
///     - index: Maps node to page
///     - last_inserted_page: None if new db instance
///         * TODO: pickup pages that are not complete
///     - max_page_size: Configurable from mycelium_config.ron block_count * block_size
///     - status: Default: new
///     - tag: Arbitrarily assigned name for container
///     - working_dir: Directory location for tag. Eventually a location inside an archive.
///
#[derive(Deserialize, Serialize)]
pub struct Tag {
    pub(crate) history: History,
    pub(crate) index: SkipMap<NodeId, PageId>,
    pub(crate) last_inserted_page: SkipSet<Option<PageId>>,
    max_page_size: usize,
    pub(crate) pages: SkipMap<PageId, Page>,
    status: TagStatus,
    tag: String,
    working_dir: std::path::PathBuf,
}

impl Tag {
    /// Page container
    pub(crate) fn new(tag: &str, working_dir: PathBuf, max_page_size: usize) -> Tag {
        let history = crate::ephemeral::history::lazy_head(&working_dir, max_page_size);
        Tag {
            history,
            index: SkipMap::new(),
            last_inserted_page: SkipSet::new(),
            status: TagStatus::New,
            max_page_size,
            pages: SkipMap::new(),
            tag: String::from(tag),
            working_dir,
        }
    }

    pub(crate) fn add_index(&self, node_id: NodeId, page_id: PageId) {
        self.index.insert(node_id, page_id);
    }

    pub(crate) fn all_container_nodes(&self) -> Summary {
        let mut summary = Summary::new();
        for page in self.pages.iter() {
            for n in &page.value().nodes {
                summary.items.push((
                    self.tag.to_string(),
                    n.value().get_id(),
                    Some(n.value().clone()),
                ));
            }
        }
        summary
    }

    pub(crate) fn all_container_nodes_map(&self) -> HashMap<NodeId, Node> {
        let mut map = HashMap::new();
        for page in self.pages.iter() {
            for node in &page.value().nodes {
                map.insert(node.value().get_id(), node.value().clone());
            }
        }
        map
    }

    pub(crate) fn clear(&self) {
        self.index.clear();
        self.pages.clear();
        self.history.clear();
    }

    /// Use only on pages known to exist. No checks will panic.
    pub(crate) fn get_append_page_id(&self, node_size: usize) -> [u8; 16] {
        if let Some(lip) = self.last_inserted_page.pop_front() {
            if let Some(page_id) = lip.value() {
                if let Some(page_ref) = self.pages.get(page_id) {
                    if page_ref.value().get_size() + node_size < page_ref.value().get_max_size() {
                        return page_ref.value().get_id();
                    }
                }
            }
        }

        // get a new page either the other is full or we have not inserted yet
        let new_page = create_page(self.tag.as_str(), self.max_page_size);
        let new_page_id = new_page.get_id();
        self.pages.insert(new_page_id, new_page);
        return new_page_id;
    }

    pub(crate) fn get_all_nodes(&self) -> Summary {
        let summary = Summary::new().with_result_list(
            self.pages
                .iter()
                .par_bridge()
                .flat_map(|page| page.value().get_nodes().clone())
                .collect(),
        );
        summary
    }

    pub(crate) fn get_node(&self, id: NodeId) -> Summary {
        if let Some(target_page) = self.index.get(&id) {
            if let Some(page) = &self.pages.get(target_page.value()) {
                for (tag, p_id, node) in page.value().get_nodes() {
                    if p_id == id {
                        return Summary::new().with_result_list(vec![(tag, p_id, node)]);
                    }
                }
            }
        }
        Summary::new().with_error("Error: Id not found")
    }

    #[allow(dead_code)]
    pub(crate) fn get_tag(&self) -> &str {
        self.tag.as_str()
    }

    pub(crate) fn is_loaded(&self) -> bool {
        self.status == TagStatus::Loaded
    }

    pub(crate) fn load(&self) -> Self {
        let mut container = Tag::new(
            self.tag.as_str(),
            self.working_dir.to_path_buf(),
            self.max_page_size,
        );
        if !&self.working_dir.exists() {
            return Tag::new(
                self.tag.as_str(),
                self.working_dir.to_path_buf(),
                self.max_page_size,
            );
        }
        if !&self.working_dir.exists() {
            return Tag::new(
                self.tag.as_str(),
                self.working_dir.to_path_buf(),
                self.max_page_size,
            );
        }

        let pages: Vec<PathBuf> = fs::read_dir(&self.working_dir)
            .expect("Error reading container.")
            .par_bridge()
            .filter_map(|file| {
                let file = file.expect("Error reading item from container.");
                let path = file.path();
                if path.extension().is_none() && path.is_file() {
                    Some(path)
                } else {
                    None
                }
            })
            .collect();

        container.pages = load_from_vec(pages).unwrap();
        container.status = TagStatus::Loaded;
        container
    }

    pub(crate) fn replace_node(&self, node_id: NodeId, node: Node) -> std::io::Result<()> {
        if let Some(id_map) = self.index.get(&node_id) {
            let page_id = id_map.value();
            if let Some(page) = self.pages.get(page_id) {
                if let Some(_) = page.value().nodes.remove(&node_id) {
                    page.value().nodes.insert(node_id, node);
                }
            }
        }
        Ok(())
    }

    pub(crate) fn save_all(&self) -> std::io::Result<()> {
        crate::persistence::prep_tag_directory(&self.working_dir)?;
        self.pages.iter().par_bridge().for_each(|page| {
            match page.value().save(&self.working_dir) {
                Ok(_) => (),
                Err(e) => panic!("Failed to save page: {:?}", e),
            }
        });
        self.history.save(&self.history.get_working_directory())?;
        self.history
            .pages
            .iter()
            .par_bridge()
            .for_each(|history_page| {
                match history_page
                    .value()
                    .save(&self.history.get_working_directory())
                {
                    Ok(_) => (),
                    Err(e) => panic!("Failure saving page: {:?}", e),
                }
            });

        Ok(())
    }

    pub(crate) fn update_node(&self, node_id: NodeId, item: &[u8]) -> Summary {
        let mut original = self.get_node(node_id);
        if let Some(new) = original.items.first() {
            if let Some(node) = new.2.clone() {
                let replace = node.create_update_node(item);
                match self.replace_node(node_id, replace) {
                    Ok(_) => (),
                    Err(e) => original
                        .errors
                        .push(format!("Failed to update node: {:?}", e)),
                }
            }
        }
        original
    }

    pub(crate) fn update_node_history(&self, node: Node) -> Result<(), Box<dyn std::error::Error>> {
        self.history.append_history(node)
    }

    /// Index all items so we know what page contains them
    /// when looking for a specific item.
    pub(crate) fn with_page_item_index(mut self) -> Self {
        let index: SkipMap<NodeId, PageId> = SkipMap::new();
        for page in self.pages.iter() {
            for node in page.value().get_nodes() {
                index.insert(node.1, *page.key());
            }
        }
        self.index = index;
        self
    }
}
