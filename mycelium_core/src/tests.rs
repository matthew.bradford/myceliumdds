use super::*;
use rayon::prelude::*;

#[derive(Serialize, Deserialize)]
struct TestItem {
    field1: String,
    field2: i32,
    field3: i64,
    field4: u32,
    field5: usize,
    field6: f32,
    field7: std::ops::Range<i32>,
}

#[test]
fn empty_test() {
    let db = Db::new().expect("Failed to create db");
    let db_emtpy = db.empty();

    assert!(db.working_dir == db_emtpy.working_dir);
}

#[test]
fn get_item_test() {
    let stew = Db::init(None).expect("Failed to init stew in get_item_test()");
    let id = stew.add("Test get_item_test()".as_bytes(), "get_item_test");
    let _item = stew.get("get_item_test", id.items.first().unwrap().1);

    assert!(true)
}

#[test]
fn list_tags_test() {
    let stew = Db::init(None).expect("Failed to init stew in list_tags_test()");
    let _summary = stew.add("some bytes".as_bytes(), "list_tags_test");

    let tags = stew.list_tags();
    if tags.len() == 1 {
        let test = tags.first().unwrap();
        if test != "list_tags_test" {
            assert!(false, "wrong tag in list_tags_test.")
        }
    } else {
        assert!(false, "Wrong number of tags.")
    }

    assert!(true)
}

#[test]
fn refresh_test() {
    let stew = Db::init(None).expect("Failed to init stew in refresh_test()");
    let item_id = stew.add("Refresh Test Item".as_bytes(), "refresh_test");
    match stew.refresh("refresh_test") {
        Ok(_) => {}
        Err(e) => assert!(false, format!("Failed to refresh DB: {:?}", e)),
    }
    let item = stew.get("refresh_test", item_id.items.first().unwrap().1);
    assert!(item.items.len() == 0);
}

#[test]
fn insert_1k_concurrent_mcore() {
    let tag = "insert_10k_concurrent";
    let mut dir = dirs::home_dir().expect("No home dir");
    dir.push(".mycelium_1ktest");
    let config = Config::new().with_data_directory_pathbuf(dir);
    let db = Db::init(Some(config)).expect("Failed to init stew in refresh_test()");
    db.load_tag(Action::Default, tag);

    let mut values = vec![];
    for i in 0..1000 {
        values.push(format!("a duck in a pickup: {}", i).as_bytes().to_vec());
    }

    let ids: Vec<[u8; 16]> = values
        .par_iter()
        .map(|x| {
            let result = db.add(x.as_slice(), tag);
            result.items.first().expect("Failed to get summary item").1
        })
        .collect();

    let mut failed: Vec<(String, [u8; 16])> = vec![];
    for id in ids {
        if let Some(_) = db.get(tag, id).items.first() {
        } else {
            failed.push((tag.to_string(), id));
        }
    }
    if failed.len() > 0 {
        assert!(false, format!("Items not found: {:?}", failed));
    }

    match db.save_all() {
        Ok(_) => {}
        Err(e) => {
            let msg = format!("Failed to save DB: {:?}", e);
            assert!(false, msg);
        }
    }
}
