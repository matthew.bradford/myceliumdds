use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

use crate::ephemeral::history::History;

pub fn read_head(path: &PathBuf) -> Option<History> {
    if path.exists() && path.is_file() {
        let file = match File::open(path) {
            Ok(f) => Some(f),
            Err(_) => None,
        };

        let mut buffer = Vec::new();
        if file.is_some() {
            let mut file = file.unwrap();
            match file.read_to_end(&mut buffer) {
                Ok(_) => (),
                Err(e) => {
                    error!("Error: {:?}", e);
                    return None;
                }
            }
            let hist = match bincode::deserialize(buffer.as_slice()) {
                Ok(h) => h,
                Err(e) => panic!("Failed to deserialize history: {:?}", e),
            };
            buffer.clear();
            Some(hist)
        } else {
            None
        }
    } else {
        None
    }
}
