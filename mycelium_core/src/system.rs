use std::path::PathBuf;

use crate::{fetch::Fetchable, DbId};
use serde::de::DeserializeOwned;
use uuid;

pub const FILE_NAME: &str = "system.bin";

#[derive(Clone, Serialize, Deserialize)]
pub struct System {
    pub db_id: DbId,
}

impl System {
    #[allow(dead_code)]
    pub fn get_system_id(&self) -> DbId {
        self.db_id
    }
}

impl Fetchable for System {
    fn deserialize_l<T: DeserializeOwned + Default + Fetchable>(
        f_path: &PathBuf,
    ) -> std::io::Result<T> {
        System::deserialize_bin(f_path)
    }

    fn serialize_l(&self) -> std::io::Result<Vec<u8>>
    where
        Self: serde::Serialize + Fetchable,
    {
        self.serialize_bin()
    }
}

impl Default for System {
    fn default() -> System {
        System {
            db_id: *uuid::Uuid::new_v4().as_bytes(),
        }
    }
}
