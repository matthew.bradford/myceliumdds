use crate::Db;
use mycelium_command::prelude::*;

pub fn vacuum(db: Db) -> std::io::Result<Db> {
    let clean_db = db.empty();
    db.primary.iter().for_each(|tag| {
        for (_, _, node) in tag.value().get_all_nodes().items {
            if let Some(n) = node {
                crate::core::p_append(&clean_db, Action::Default, tag.key(), n);
            }
        }
    });

    clean_db.save_all()?;
    Ok(clean_db)
}
