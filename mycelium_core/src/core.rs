use std::fs;
use std::path::PathBuf;

use rayon::prelude::*;

use crate::ephemeral::Tag;
use crate::persistence::SaveScope;
use crate::Db;
use mycelium_command::prelude::*;
use mycelium_experimental::prelude::*;

pub(crate) fn add(db: &Db, item: &[u8], tag: &str) -> Summary {
    let node = Node::new(db.system.db_id, item);
    p_append(db, Action::Insert(None), tag, node.clone())
}

pub(crate) fn p_append(db: &Db, action: Action, tag_id: &str, node: Node) -> Summary {
    let mut summary = validate_tag(db, action, tag_id);
    if let Some(tag) = db.primary.get(tag_id) {
        let page_id = tag.value().get_append_page_id(node.get_size());
        if let Some(page) = tag.value().pages.get(&page_id) {
            let id = page.value().add(node.clone());
            tag.value().add_index(node.get_id(), id);
            tag.value().last_inserted_page.insert(Some(page_id));

            summary
                .schedule
                .push(Notify::CreateNode(tag_id.to_string(), node.get_id()));
            summary
                .items
                .push((tag_id.to_string(), node.get_id(), Some(node)));

            return summary;
        }
    }

    summary
        .errors
        .push(format!("Tag with id: {} not found", tag_id));
    summary
}

pub(crate) fn validate_tag(db: &Db, actn: Action, tag_id: &str) -> Summary {
    let mut summary = Summary::new();
    // Get tag structure or create a new one. It is valid if it
    // exists.
    let mut new_tag = false;
    if let Some(_) = db.primary.get(tag_id) {
        // Tag is valid
    } else {
        match actn {
            Action::Insert(_) => {
                new_tag = true;
            }
            _ => (),
        }
    }
    if new_tag {
        summary.append(&mut create_tag(db, tag_id));
    }
    summary
}

pub(crate) fn create_tag(db: &Db, tag_id: &str) -> Summary {
    let mut summary = Summary::new();
    let mut tag_working_dir = db.working_dir.to_path_buf();
    tag_working_dir.push(tag_id);
    db.primary.insert(
        tag_id.to_string(),
        Tag::new(tag_id, tag_working_dir, db.config.get_max_page_size()),
    );
    summary.schedule.push(Notify::CreateTag(tag_id.to_string()));
    summary
}

pub(crate) fn load_tag_containers(
    working_dir: &PathBuf,
    max_page_size: usize,
) -> Result<SkipMap<String, Tag>, std::io::Error> {
    let map = SkipMap::new();
    if working_dir.exists() {
        let directories = fs::read_dir(working_dir)?;
        for item in directories {
            let item = item?;
            let path = item.path();
            if path.is_dir() {
                let parts = path.components();
                let tag = parts.last().unwrap().as_os_str().to_str().unwrap();
                map.insert(String::from(tag), Tag::new(tag, item.path(), max_page_size));
            }
        }
    }
    Ok(map)
}

pub(crate) fn save_all(db: &Db) -> Result<(), std::io::Error> {
    save_pages(db, SaveScope::All);
    Ok(())
}

fn save_pages(db: &Db, scope: SaveScope) {
    match scope {
        SaveScope::All => {
            db.primary
                .iter()
                .par_bridge()
                .for_each(|tag| match tag.value().save_all() {
                    Ok(_) => (),
                    Err(e) => error!("Error: {:?}", e),
                });
        }
        SaveScope::One(_item) => unimplemented!(),
        SaveScope::Some(_items) => unimplemented!(),
        SaveScope::Tag(_tag) => unimplemented!(),
    }
}
