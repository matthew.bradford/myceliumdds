# ------------------------------------------------------------------------------
# Cargo Build Stage
# ------------------------------------------------------------------------------

# FROM rust:latest as cargo-build

# RUN apt-get update

# RUN apt-get install musl-tools -y

# RUN rustup target add x86_64-unknown-linux-musl

# WORKDIR /usr/src/mycelium

# COPY . .
# COPY Cargo-docker.toml Cargo.toml

# RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl

# RUN rm -f target/x86_64-unknown-linux-musl/release/deps/mycelium

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------

 FROM alpine:latest

 #COPY --from=cargo-build /usr/src/mycelium/target/x86_64-unknown-linux-musl/release/mycelium /usr/local/bin/mycelium
 COPY target/x86_64-unknown-linux-musl/release/mycelium /usr/local/bin/mycelium

 EXPOSE 34120
 EXPOSE 34121

 CMD ["mycelium"]


