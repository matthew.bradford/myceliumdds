use std::io::{BufRead, BufReader, Write};
use std::net::TcpStream;

pub fn tcp_call_resposne<S>(ip: S, port: S, request: String) -> Result<String, std::io::Error>
where
    S: Into<String>,
{
    let addr = format!("{}:{}", ip.into(), port.into());
    TcpStream::connect(&addr).and_then(|mut stream| {
        let mut request = request;
        request.push('\n');
        stream.write_all(request.as_bytes()).map_err(|x| x)?;
        // Todo: Server bug or client bug where stream is not terminated correctly?
        stream.shutdown(std::net::Shutdown::Write)?;

        let mut buffer = vec![];
        let mut reader = BufReader::new(&stream);
        reader
            .read_until(b'\n', &mut buffer)
            .expect("Failed to read");
        Ok(String::from_utf8(buffer).expect("Couldnt convert"))
    })
}
