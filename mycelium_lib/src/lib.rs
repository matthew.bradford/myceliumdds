extern crate bincode;
extern crate bytes;
extern crate chrono;
extern crate crossbeam_deque as deque;
extern crate crossbeam_queue as queue;
extern crate fern;
extern crate mycelium_command;
extern crate mycelium_experimental;
extern crate mycelium_index;
extern crate ron;
#[macro_use]
extern crate serde;
extern crate serde_json;
extern crate socket2;
extern crate uuid;

mod client;
pub mod prelude;
mod tcp_server;
mod udp_server;

use std::net::Ipv4Addr;
use std::sync::Arc;

use crate::prelude::*;
use mycelium_command::prelude::*;
use mycelium_experimental::crossbeam_skiplist::map::SkipMap;
use mycelium_experimental::crossbeam_skiplist::set::SkipSet;

///
/// # Mycelium
///
/// Primary method to embed database into applications is
/// through this struct.
///
/// TCP, UDP, or calling functions implemented on Mycelium
/// in this library to interact with the database.
///
/// TCP (localhost) is not required. UDP server
/// should be started for distributed behavior, but is not
/// required if that behavior is not desired.
///
#[allow(dead_code)]
pub struct Mycelium {
    pub config: Config,
    data: Index,
    network: SkipMap<String, SkipSet<(Ipv4Addr, usize, usize)>>,
    tcp_server: bool,
    udp_server: bool,
}

impl Mycelium {
    ///
    /// # Init_db
    ///
    /// Initialize a new instance of Mycelium
    ///
    /// ```
    /// use mycelium_lib::prelude::*;
    ///
    /// let config = Config::default()
    ///  .with_data_directory("./");
    ///
    /// let db = Mycelium::init_db(config, false, false);
    ///
    /// ```
    ///
    pub fn init_db(
        config: Config,
        cnfg_tcp_server: bool,
        cnfg_udp_server: bool,
    ) -> std::sync::Arc<Mycelium> {
        let idx = Index::init(Some(config.clone())).expect("Failed to init db.");
        Arc::new(Mycelium {
            config,
            data: idx,
            network: SkipMap::new(),
            tcp_server: cnfg_tcp_server,
            udp_server: cnfg_udp_server,
        })
    }

    pub fn config(&self) -> Config {
        self.config.clone()
    }

    ///
    /// Embedded option
    ///
    /// ```
    /// use mycelium_command::prelude::*;
    /// use mycelium_lib::{ prelude::*, Mycelium };
    /// use mycelium_index::prelude::Config;
    ///
    /// let config = Config::fetch_or_default(&std::env::current_exe().expect("a path"))
    ///     .expect("failed to get a default config");
    /// let db = Mycelium::init_db(config.0, true, true);
    /// let cmd = Command::new()
    ///     .with_action(Action::Insert(Some(b"Mastiff, Pyranese, Shepard...
    ///         I do not know my dogs well.".to_vec())))
    ///     .with_tag("dog")
    ///     .with_what(What::Index("working".to_string()));
    ///
    /// match db.execute_command(cmd) {
    ///     Ok(_) => assert!(true),
    ///     Err(_) => assert!((false)),
    /// }
    /// ```
    ///
    pub fn execute_command(
        &self,
        cmd: Command,
    ) -> std::result::Result<ReturnPkg, Box<dyn std::error::Error>> {
        match &cmd.action {
            Action::Default => Ok((None, None)),
            Action::Archive(_id) => unimplemented!(), // TODO: Archive item with id
            Action::Insert(Some(item)) => {
                let ops: Vec<&str> = cmd
                    .ops
                    .iter()
                    .filter(|(_op, v)| match v {
                        What::Index(_) => true,
                        _ => false,
                    })
                    .map(|(op, what)| {
                        match op {
                            LogicOp::And => match what {
                                What::Index(s) => s.as_str(),
                                _ => "",
                            },
                            _ => unimplemented!(), // TODO: OR
                        }
                    })
                    .collect();

                let summary;
                if ops.is_empty() {
                    summary = self.data.add(&cmd.tag, item);
                } else {
                    summary = self.data.add_indexed(&cmd.tag, item, &ops);
                }

                self.data.save_all()?;
                let first = summary.items.first().unwrap();
                let mut item = None;
                if let Some(node) = &first.2 {
                    item = Some(node.get_item());
                }
                Ok((Some(vec![(first.0.to_string(), first.1, item)]), None))
            }
            Action::Insert(None) => {
                let summary = self.data.add(&cmd.tag, &vec![]);
                self.data.save_all()?;
                let first = summary.items.first().unwrap();
                let mut item = None;
                if let Some(node) = &first.2 {
                    item = Some(node.get_item());
                }
                Ok((Some(vec![(first.0.to_string(), first.1, item)]), None))
            }
            Action::Select => {
                // Would it be valid for ops AND index to exist in the same statement as
                // one with a where ID is? SELECT FROM dog WHERE ID IS {x} AND working ??
                // ... It might be ... It looks like it says something like get me the dog with
                // this ID if it is in the working dog index. Otherwise it is invalid.
                let mut and: Vec<&str> = vec![];
                let mut or: Vec<&str> = vec![];
                let mut ids: Vec<[u8; 16]> = vec![];
                self.data.db.load_tag(Action::Default, &cmd.tag);
                cmd.ops.iter().for_each(|(op, what)| {
                    let v = what.get_value();
                    if let Some(id) = v.0 {
                        ids.push(id);
                    }
                    match op {
                        LogicOp::And => {
                            if let Some(idx) = v.1 {
                                and.push(idx);
                            }
                        }
                        LogicOp::Or => {
                            if let Some(idx) = v.1 {
                                or.push(idx);
                            }
                        }
                        _ => unimplemented!(),
                    }
                });

                // Get index search
                let mut summary = self.data.search(&and, &or, cmd.map);

                // Get nodes by id
                if ids.len() > 0 {
                    let mut result_id = self.data.get(cmd.tag.as_str(), &ids);
                    summary.append(&mut result_id);
                }

                Ok(summary.to_return_package())
            }
            Action::Update((_id, _item)) => unimplemented!(),
        }
    }
}
