use std::io::BufReader;
use std::net::SocketAddr;
use std::sync::Arc;

use tokio::io::{lines, write_all};
use tokio::net::TcpListener; //TcpStream
use tokio::prelude::*;

use crate::prelude::*;
use mycelium_command::ReturnPkg;

///
/// # Local TCP server
/// Either this (msql or serialized CommandAST over TCP loopback)
///
/// ```
/// use mycelium_command::prelude::*;
/// use mycelium_lib::{ prelude::*, Mycelium };
/// use mycelium_index::prelude::Config;
///
/// let config = Config::fetch_or_default(&std::env::current_exe().expect("a path"))
///     .expect("failed to get a default config");
/// let db = Mycelium::init_db(config.0, true, true);
///
/// //match lib_mycelium::start_local(db) {
/// //    Ok(_) => (),
/// //    Err(e) => {
/// //        panic!("Failed to start db server: {:?}" e);
/// //    }
/// //}
///
/// ```
///
pub fn start_local(db: Arc<Mycelium>) -> std::io::Result<()> {
    let port = db.config.tcp_port;
    let ip = "0.0.0.0";
    let addr = std::env::args()
        .nth(1)
        .unwrap_or_else(|| format!("{}:{}", ip, port));
    let addr = addr.parse::<SocketAddr>().expect("socket error");
    let listener = TcpListener::bind(&addr)
        .map_err(|_| "Failed to bind socket.")
        .expect("Failed to bind listener");

    let db_listener = db.clone();
    let done = listener
        .incoming()
        .map_err(|e| println!("Error accepting socket: error = {:?}", e))
        .for_each(move |socket| {
            let (reader, writer) = socket.split();
            let lines = lines(BufReader::new(reader));
            let db_listener_clone = db_listener.clone();
            let responses = lines.map(move |line| {
                let request = match Request::parse(&line) {
                    Ok(req) => req,
                    Err(e) => return Response::Error { msg: e },
                };

                match request {
                    Request::Command { cmd } => Response::Value {
                        pkg: db_listener_clone.execute_command(cmd).unwrap(),
                    },
                    Request::ParseSql { cmd } => Response::Value {
                        pkg: db_listener_clone.execute_command(cmd).unwrap(),
                    },
                }
            });

            let writes = responses.fold(writer, |writer, response| {
                let mut response = response.serialize();
                response.push('\r');
                response.push('\n');
                write_all(writer, response.into_bytes()).map(|(w, _)| w)
            });

            let msg = writes.then(move |_| Ok(()));
            tokio::spawn(msg)
        });

    tokio::run(done);
    Ok(())
}

#[allow(dead_code)]
enum ResponseFormat {
    BIN,
    JSON,
    RON,
}

#[allow(dead_code)]
enum Request {
    // Serialized mycelium_command::Command
    Command { cmd: Command },
    // Parse sql to mycelium_command::Command
    ParseSql { cmd: Command },
}

impl Request {
    fn parse(input: &str) -> std::result::Result<Request, String> {
        if input.len() > 3 {
            let head = input.split_at(4);
            match head.0.trim() {
                "cmd" => match serde_json::from_str(head.1) {
                    Ok(cmd) => Ok(Request::Command { cmd }),
                    Err(_) => Err(From::from("Bad Command.")),
                },
                "msql" => match mycelium_command::Command::parse(head.1) {
                    Ok(cmd) => Ok(Request::Command { cmd }),
                    Err(e) => Err(format!("Parse Error: {}", e)),
                },
                _ => Err(From::from("Unknown statement.")),
            }
        } else {
            Err(From::from("no head"))
        }
    }
}

#[allow(dead_code)]
enum Response {
    Value { pkg: ReturnPkg },
    Error { msg: String },
}

impl Response {
    fn serialize(&self) -> String {
        match *self {
            Response::Value { ref pkg } => format!(
                "{}",
                serde_json::to_string(pkg).expect("Fatal: Failed to Serialize")
            ),
            Response::Error { ref msg } => format!("error: {}", msg),
        }
    }
}
