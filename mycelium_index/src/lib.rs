extern crate mycelium_command;
extern crate mycelium_core;
extern crate mycelium_experimental;
extern crate rayon;
#[macro_use]
extern crate serde;

use std::path::PathBuf;

use rayon::prelude::*;
use serde::{de::DeserializeOwned, Serialize};

use mycelium_core::prelude::*;
use mycelium_core::{
    fetch::{Container, Fetchable},
    DbId,
};

use crate::index::SkipIndex;
use mycelium_command::prelude::*;
use mycelium_experimental::crossbeam_skiplist::map::Entry;
use mycelium_experimental::crossbeam_skiplist::map::SkipMap;

mod index;
pub mod prelude;

pub const IDX_NAME: &str = "map.idx";

///
/// # Index
/// Will hold multiple types of indexes back into Db
/// Map<"Index Term", SkipIndex>
///
pub struct Index {
    pub db: Db,
    idx_map: IndexMap,
}

///
/// # IndexMap
///
/// Structure for indexes. Contains a map of Index to SkipIndex.
/// SkipIndex maps tags and Id's. A index may have items related
/// between tags.
///
#[derive(Debug, Serialize, Deserialize)]
pub struct IndexMap {
    working_dir: PathBuf,
    map: SkipMap<String, SkipIndex>,
}

impl Default for IndexMap {
    fn default() -> IndexMap {
        IndexMap {
            working_dir: Default::default(),
            map: Default::default(),
        }
    }
}

impl IndexMap {
    fn new(path: PathBuf) -> IndexMap {
        IndexMap {
            working_dir: path,
            map: Default::default(),
        }
    }

    fn contains_index(&self, key: &str) -> bool {
        self.map.contains_key(key)
    }

    fn get(&self, key: &str) -> Option<Entry<String, SkipIndex>> {
        self.map.get(key)
    }

    fn insert(&self, key: &str, skip_idx: SkipIndex) -> Entry<String, SkipIndex> {
        self.map.insert(key.to_string(), skip_idx)
    }
}

impl Container for IndexMap {
    fn get_file_name(&self) -> &str {
        IDX_NAME
    }

    fn get_file_path(&self) -> &PathBuf {
        &self.working_dir
    }
}

impl Fetchable for IndexMap {
    fn deserialize_l<T: DeserializeOwned + Default + Fetchable>(
        f_path: &PathBuf,
    ) -> std::io::Result<T> {
        IndexMap::deserialize_bin(f_path)
    }

    fn serialize_l(&self) -> std::io::Result<Vec<u8>>
    where
        Self: serde::Serialize + Fetchable,
    {
        self.serialize_bin()
    }
}

impl Index {
    ///
    /// # Init
    /// Initialize containers. If first run a system folder will be
    /// created otherwise nothing else is done.
    ///
    /// * Parameters
    ///     - config: Config for Db defaults to Config::default()
    ///
    /// * Return
    ///     - Result: Index
    ///
    /// ```
    /// use crate::mycelium_index::prelude::*;
    ///
    /// match Index::init(None) {
    ///     Ok(_) => assert!(true),
    ///     Err(_) => assert!(false, "Error creating db")
    /// }
    /// ```
    ///
    pub fn init(config: Option<Config>) -> std::io::Result<Index> {
        let db = match mycelium_core::Db::init(config) {
            Ok(s) => s,
            Err(e) => panic!("Failed to initialize stew: {:?}", e),
        };
        let working_dir = db.get_working_dir();
        let mut map = IndexMap::new(working_dir.to_path_buf()).fetch_self();
        if map.working_dir != working_dir {
            map.working_dir = working_dir;
        }
        Ok(Index { db, idx_map: map })
    }

    ///
    /// # Add
    /// Add item to database
    ///
    /// * Parameters
    ///     - tag: container id to add item to
    ///     - item: byte array of item
    ///     - type_name: optional type id
    ///
    /// * Return
    ///     - Result: [u8;16] id of item added to db/index
    ///
    /// ```
    /// use crate::mycelium_index::prelude::*;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let idb = Index::init(None).expect("Failed to create idb");
    /// // add item
    /// let id = idb.add("dog", b"shepard");
    ///
    /// assert!(id.items.first().unwrap().1 != [0;16])
    ///
    /// ```
    ///
    pub fn add(&self, tag: &str, item: &[u8]) -> Summary {
        self.db.add(item, tag)
    }

    ///
    /// # Add Indexed
    /// Add an item to the database and any implemented indexes with given
    /// association.
    ///
    /// * Parameters
    ///     - tag: container id to put item in
    ///     - item: byte array of item
    ///     - hit_box: index terms this item should be associated
    ///         with ["fish", "dog"]
    ///
    /// * Return
    ///     - Result: [u8; 16] unique db id of item added
    ///
    /// ```
    /// use crate::mycelium_index::prelude::*;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let idb = Index::init(None).expect("Failed to create idb");
    /// // add item with index associates
    /// let summary = idb.add_indexed(
    ///         "dog",
    ///         b"good boy",
    ///         &["shepherd", "working dog", "pet"]);
    ///
    /// let id = summary.items.first().unwrap().1;
    ///
    /// assert!(id != [0 as u8;16])
    /// ```
    ///
    #[allow(unused_assignments)]
    pub fn add_indexed(&self, tag: &str, item: &[u8], hit_box: &[&str]) -> Summary {
        let summary = self.db.add(item, tag);
        let (_tag, id, _node) = summary.items.first().unwrap();
        for idx in hit_box {
            let mut result = None;
            let key = idx.to_string();
            if !self.idx_map.contains_index(&key) {
                result = Some(self.idx_map.insert(idx, SkipIndex::new()));
            } else {
                result = self.idx_map.get(&key);
            }

            if let Some(r) = result {
                r.value().upsert(tag, *id);
            }
        }

        summary
    }

    ///
    /// # Get
    /// Get an item/items from container
    ///
    /// * Parameters
    ///     - tag: container id
    ///     - ids: array of ids to retrieve [[u8;16],[u8;16]
    ///
    /// * Return
    ///     - Option: Vector of matches
    ///
    /// ```
    /// use crate::mycelium_index::prelude::*;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let idb = Index::init(None).expect("Failed to create db");
    /// // add item
    /// let id = idb.add("tag", b"some stuff").items.first().unwrap().1;
    /// let id2 = idb.add("tag", b"some stuff 2").items.first().unwrap().1;
    /// let id3 = idb.add("tag", b"some stuff 3").items.first().unwrap().1;
    /// // get item
    /// let f_item = idb.get("tag", &[id, id3]);
    ///
    /// assert!(f_item.items.len() == 2);
    /// let ids: Vec<[u8;16]> = f_item.items.iter().map(|(_, id, _)| *id).collect();
    /// assert!(ids.contains(&id) && ids.contains(&id3) && !ids.contains(&id2));
    ///
    /// ```
    ///
    pub fn get(&self, tag: &str, ids: &[DbId]) -> Summary {
        let mut summary = Summary::new();
        for id in ids {
            summary.append(&mut self.db.get(tag, *id));
        }
        summary
    }

    pub fn get_system_id(&self) -> DbId {
        self.db.get_system_id()
    }

    pub fn list_tags(&self) -> Vec<String> {
        self.db.list_tags()
    }

    ///
    /// # Load Tag
    /// Load a tag before performing operations over it.
    /// Destructive if called over loaded tag drop current and load from disk.
    ///
    pub fn load_tag(&mut self, actn: Action, tag: &str) -> Summary {
        self.db.load_tag(actn, tag)
    }

    ///
    /// # Search
    /// Search for items in container.
    ///
    /// * Parameters:
    ///     - tag: container id
    ///     - and: search items
    ///     - or:  search items
    ///
    /// * Result:
    ///     - Option: List of nodes that are associated with hit_list items.
    ///         TODO: Order by match count
    ///
    /// ```
    /// use crate::mycelium_index::prelude::*;
    /// use mycelium_command::prelude::*;
    ///
    /// // new db
    /// let idb = Index::init(None).expect("Failed to create db");
    /// // add some nodes
    /// let sum_mut = idb.add_indexed("tag", b"Zippy", &["Shepherd", "Husky", "Akita", "Lab"]);
    /// let sum_boxer = idb.add_indexed("tag", b"Trip", &["Boxer"]);
    /// let sum_mut2 = idb.add_indexed("tag", b"Buddy", &["Boxer", "Lab"]);
    /// let sum_pointer = idb.add_indexed("tag", b"Sam", &["Pointer"]);
    ///
    /// // Or's...
    /// let dogs = idb.search(&[], &["Boxer", "Lab"], false);
    /// assert!(dogs.items.len() == 3);
    /// // case sensitive
    /// let dog = idb.search(&[], &["pointer"], false);
    /// assert!(dog.items.len() == 0);
    /// let dog = idb.search(&[], &["Pointer"], false);
    /// assert!(dog.items.len() == 1);
    ///
    /// // And's...
    /// let dogs = idb.search(&["Boxer", "Lab"], &[], false);
    /// assert!(dogs.items.len() == 1);
    /// let dogs = idb.search(&["Boxer", "Pointer"], &[], false);
    /// assert!(dogs.items.len() == 0);
    /// ```
    ///
    pub fn search(&self, and: &[&str], or: &[&str], map: bool) -> Summary {
        let or_items = self.search_inclusive(or);
        let and_items = self.search_intersect(and);
        let mut items = vec![];
        if let Some(mut or) = or_items {
            items.append(&mut or);
        }
        if let Some(mut and) = and_items {
            items.append(&mut and)
        }

        let mut summary = Summary::new();
        // If no results shortcut method
        if items.len() == 0 {
            summary.messages.push("No results".to_string());
            return summary;
        }

        if map {
            let result: Vec<Summary> = items
                .par_iter()
                .map(|(tag, id)| self.db.get(&tag, *id))
                .collect();

            for mut s in result {
                summary.append(&mut s);
            }
        } else {
            let mut result = items.par_iter()
                .map(|(tag, id)| (tag.to_string(), *id, None))
                .collect();
            summary.items.append(&mut result);
        }

        summary
    }

    ///
    /// # Search Intersect
    /// Search items in container. Item intersection as item is in index1,
    /// index2, and index3. This is an and op search.
    ///
    fn search_intersect(&self, ands: &[&str]) -> Option<Vec<(String, DbId)>> {
        let mut results = None;
        let mut lands = ands.to_vec();
        // ands has a result
        if let Some(first) = lands.pop() {
            // get first index
            if let Some(first_idx) = self.idx_map.get(first) {
                let mut tmp = vec![];
                // if we have more than one index to check for intersection
                if lands.len() > 0 {
                    // Go through all nodes in the first index
                    let v: Vec<Option<(String, [u8; 16])>> = first_idx
                        .value()
                        .get()
                        .iter()
                        .par_bridge()
                        .map(|(tag, id)| {
                            // Check if value exists in other indexes
                            let mut add = Some((tag.to_string(), *id));
                            for and in ands {
                                if let Some(other_index) = self.idx_map.get(*and) {
                                    if !other_index.value().get().contains(&(tag.to_string(), *id))
                                    {
                                        add = None;
                                        break;
                                    }
                                }
                            }
                            add
                        })
                        .collect();

                    for item in v {
                        if let Some(i) = item {
                            tmp.push(i);
                        }
                    }
                } else {
                    tmp = first_idx.value().get();
                }

                if tmp.len() > 0 {
                    results = Some(tmp);
                }
            }
        }

        results
    }

    ///
    /// # Search
    /// Search items in container.
    /// Returns items in this or this or this. This is an or op search.
    ///
    fn search_inclusive(&self, ors: &[&str]) -> Option<Vec<(String, DbId)>> {
        let mut result = Vec::new();
        for or in ors {
            if let Some(idx) = self.idx_map.get(*or) {
                let mut lst = ors.par_iter().flat_map(|_| idx.value().get()).collect();
                result.append(&mut lst);
            }
        }

        if result.len() > 0 {
            result.sort();
            result.dedup();
            Some(result)
        } else {
            None
        }
    }

    pub fn save_all(&self) -> std::io::Result<()> {
        self.db.save_all()?;
        self.idx_map.save()?;
        Ok(())
    }
}

///
/// # Indexing
///
pub trait Indexed {
    /// Add a relationship between this item_id and this term for search and relations.
    fn add(&mut self, idx: &str, item_id: [u8; 16]) -> std::io::Result<()>;

    /// Check that container has index
    fn contains(&self, idx: &str) -> bool;

    /// Remove indexing for this term from container
    fn remove(&mut self, idx: &str) -> std::io::Result<()>;

    /// Remove item from indexed term.
    fn remove_for(&mut self, idx: &str, item_id: [u8; 16]) -> std::io::Result<()>;
}
