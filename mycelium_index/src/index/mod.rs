pub(crate) mod fm;
use mycelium_core::{
    DbId,
};
use mycelium_experimental::crossbeam_skiplist::map::SkipMap;
use mycelium_experimental::crossbeam_skiplist::set::SkipSet;
use serde::{ Serialize };

#[derive(Debug, Clone, Serialize, Deserialize)]
enum State {
    Init,
    Ready,
}

///
/// # SkipIndex
/// Map tags to nodes within a tag connected by this index.
///
/// - i: SkipMap<Tag_id, SkipSet<[u8; 16]>>
/// - working_dir: location of index file
/// - state: state of index
///
#[derive(Debug, Serialize, Deserialize)]
pub struct SkipIndex {
    // search item, list of database items related to search item
    i: SkipMap<String, SkipSet<DbId>>,
}

impl SkipIndex {
    pub fn new() -> SkipIndex {
        SkipIndex::default()
    }

    pub(crate) fn get(&self) -> Vec<(String, DbId)> {
        let mut vec = vec![];

        for map in self.i.iter() {
            for id in map.value().iter() {
                vec.push((map.key().to_string(), *id.value()))
            }
        }
        vec
    }

    pub(crate) fn upsert(&self, tag: &str, id: DbId) {
        if let Some(hit_map) = self.i.get(tag) {
            hit_map.value().insert(id);
        } else {
            // index does not exist
            let set = SkipSet::new();
            set.insert(id);
            self.i.insert(tag.to_string(), set);
        }
    }
}

#[test]
fn test_upsert() {
    use std::path::PathBuf;
    let path = PathBuf::from("");
    let idx = SkipIndex::new();
    idx.upsert("Test", [8; 16]);
    assert!(true);
}

impl Default for SkipIndex {
    fn default() -> SkipIndex {
        SkipIndex {
            i: SkipMap::default(),
        }
    }
}

#[allow(unused_variables)]
impl crate::Indexed for SkipIndex {
    // test as "duck"
    // test as "the quick brown fox jumps over the spoon"
    // test as "4"
    fn contains(&self, test: &str) -> bool {
        unimplemented!()
    }

    /// Add item w/optional string for indexing.
    /// StewId - [u8; 16] unique identifier for Mycelium
    /// Tag - Container id where the item is to be stored
    /// idx - string of terms you want this item associated with.
    ///     comma seperated "term1,term2,term3,whatever"
    fn add(&mut self, idx: &str, item_id: [u8; 16]) -> std::io::Result<()> {
        for s in idx.split(',') {
            self.add(s, item_id)?;
        }
        Ok(())
    }

    fn remove(&mut self, idx: &str) -> std::io::Result<()> {
        unimplemented!()
    }

    fn remove_for(&mut self, idx: &str, item_id: [u8; 16]) -> std::io::Result<()> {
        unimplemented!()
    }
}
