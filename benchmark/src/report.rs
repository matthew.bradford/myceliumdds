use std::io::prelude::*;
use std::io::LineWriter;

use crate::record::PerfRecord;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ReportLine {
    function_name: String,
    average_nano: u128,
    last_run_nano: u128,
    standard_dev: u128,
}

use std::cmp::Ordering;
impl std::cmp::Ord for ReportLine {
    fn cmp(&self, other: &Self) -> Ordering {
        self.function_name.cmp(&other.function_name)
    }
}

impl Eq for ReportLine {}

impl PartialOrd for ReportLine {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.function_name.cmp(&other.function_name))
    }
}

impl PartialEq for ReportLine {
    fn eq(&self, other: &Self) -> bool {
        self.function_name == other.function_name
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Report {
    pub id: usize,
    pub lines: Vec<ReportLine>,
    pub fail: bool,
}

impl Report {
    pub fn new() -> Report {
        Report {
            id: 0,
            lines: vec![],
            fail: false,
        }
    }

    pub fn line_count(&self) -> usize {
        self.lines.len()
    }

    pub fn print(&mut self) -> String {
        let mut ser_lines = String::new();
        self.lines.sort();
        ser_lines.push_str(&format!(
            "Report {}: {} lines.\n",
            self.id,
            self.line_count()
        ));
        ser_lines.push_str(&String::from("------------------------------\n"));

        for line in &self.lines {
            let dif;
            let symbol = if line.average_nano > line.last_run_nano {
                dif = line.average_nano - line.last_run_nano;
                "-"
            } else {
                dif = line.last_run_nano - line.average_nano;
                "+"
            };

            // If execution time exceeds 50% of the standard deviation mark it as a failure.
            let fifty_plus_standard = if line.standard_dev != 0 {
                line.standard_dev + (line.standard_dev / 2)
            } else {
                0
            };
            let astr = if line.last_run_nano > line.average_nano + fifty_plus_standard {
                self.fail = true;
                "*"
            } else {
                ""
            };

            ser_lines.push_str(&format!(
                "\t{}{}, ave: {}, last: {:?}, dif: {}{}, standard dev +/-: {} \n",
                astr,
                line.function_name,
                line.average_nano,
                line.last_run_nano,
                symbol,
                dif,
                line.standard_dev,
            ));
        }

        ser_lines.push_str(&String::from("------------------------------\n"));
        ser_lines.push_str(&format!("Report: {} lines.\n", self.line_count()));
        ser_lines.push_str("* - Results outside standard deviation.");
        ser_lines
    }

    pub fn update(self, ag: &PerfRecord) -> Report {
        let mut report = Report {
            id: ag.id,
            lines: vec![],
            fail: false,
        };

        for (k, v) in &ag.r {
            let mut rec_tot = 0;

            for me_dumb in &v.run_duration_nano {
                rec_tot += *me_dumb;
            }

            let rec_len = v.run_duration_nano.len() as u128;
            let ave = if rec_len > 0 { rec_tot / rec_len } else { 0 };
            let last = *v.run_duration_nano.last().unwrap();
            let sd_calc_vec = v.run_duration_nano.split_last().expect("Failed split");
            let std_dev = standard_dev(sd_calc_vec.1, ave);

            let line = ReportLine {
                function_name: k.to_string(),
                average_nano: ave,
                last_run_nano: last,
                standard_dev: std_dev,
            };

            report.lines.push(line);
        }

        report
    }

    pub fn write(mut self) -> Self {
        let f_perf =
            std::fs::File::create("result/test_perf.txt").expect("Failed to create test_perf.txt");
        let mut file = LineWriter::new(f_perf);
        file.write_all(self.print().as_bytes())
            .expect("Failed to write report data.");
        file.flush().expect("Faild to flush bench report buffer.");
        self
    }
}

fn standard_dev(v: &[u128], ave: u128) -> u128 {
    let ave_i = ave as i128;
    let mut sub_mean = vec![];
    for i in v {
        let i = *i as i128;
        let m = i - ave_i;
        let m = m * m;
        sub_mean.push(m);
    }

    let mut sub_mean_tot: i128 = 0;
    for i in &sub_mean {
        sub_mean_tot += i;
    }

    if !sub_mean.is_empty() {
        let x = sub_mean_tot / (sub_mean.len() as i128);
        let standard_dev = (x as f64).sqrt();
        standard_dev as u128
    } else {
        0
    }
}
