use std::collections::HashMap;
use std::fs::{create_dir_all, OpenOptions};
use std::io::{prelude::*, Result};
use std::path::PathBuf;
use std::str::FromStr;

use crate::Performance;
use bincode;

const RESULT_DIR: &str = "result";
const RESULT_FILE: &str = "bench.bnc";

#[derive(Debug, Deserialize, Serialize)]
pub struct PerfRecord {
    pub id: usize,
    pub r: HashMap<String, PerfRecordAg>,
}

impl PerfRecord {
    fn new() -> PerfRecord {
        PerfRecord {
            id: 0,
            r: Default::default(),
        }
    }
    pub fn save(self) -> Self {
        write(self.to_bin()).expect("Failed to write record.");
        self
    }

    pub fn to_bin(&self) -> Vec<u8> {
        bincode::serialize(self).expect("Failed to serialize report data records.")
    }
    pub fn update(mut self, current: &Performance) -> Self {
        self.id += 1;
        for x in &current.fnc_perf_results {
            let key = &x.function_name;
            if self.r.contains_key(key) {
                let rec = self.r.get_mut(key).unwrap();
                rec.run_duration_nano.push(x.duration.as_nanos());
            } else {
                self.r.insert(
                    key.to_string(),
                    PerfRecordAg {
                        run_duration_nano: vec![x.duration.as_nanos()],
                    },
                );
            }
        }
        self
    }
}

impl Default for PerfRecord {
    fn default() -> PerfRecord {
        PerfRecord {
            id: 0,
            r: HashMap::new(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PerfRecordAg {
    pub run_duration_nano: Vec<u128>,
}

pub(crate) fn fetch() -> PerfRecord {
    let mut path = PathBuf::from_str(RESULT_DIR).unwrap();
    if path.exists() {
        path.push(RESULT_FILE);
        let file = OpenOptions::new().read(true).open(&path);
        if file.is_ok() {
            let mut file = file.expect("Failed to unwrap file.");
            let mut buffer = Vec::new();
            buffer.clear();
            match file.read_to_end(&mut buffer) {
                Ok(_) => (),
                Err(e) => {
                    error!("Failed to read PerfRecord: {:?} Error: {:?}", file, e);
                    panic!("Failed to read PerfRecord: Error: {:?}", e)
                }
            }
            let sys: PerfRecord = bincode::deserialize(&buffer)
                .expect("Failed to deserialize PerfRecord in record::fetch().");
            return sys;
        }
    }

    new_record()
}

fn new_record() -> PerfRecord {
    let record = PerfRecord::new();
    write(record.to_bin()).expect("Failed to write record.");
    record
}

pub(crate) fn write(item: Vec<u8>) -> Result<()> {
    let mut file_path = PathBuf::from_str(RESULT_DIR).unwrap();
    if !file_path.exists() {
        create_dir_all(&file_path).expect("Failed to create directory for report data.");
    }
    file_path.push(RESULT_FILE);
    let mut file = match OpenOptions::new().write(true).create(true).open(&file_path) {
        Ok(f) => f,
        Err(e) => {
            error!("Failed to write/open file: {:?} Error: {:?}", file_path, e);
            panic!("Failed to write/open file: {:?} Error: {:?}", file_path, e)
        }
    };

    match file.write_all(&item) {
        Ok(_) => (),
        Err(_e) => unimplemented!(),
    }

    match file.sync_all() {
        Ok(_) => Ok(()),
        Err(_e) => unimplemented!(),
    }
}
