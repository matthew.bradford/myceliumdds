extern crate bincode;
extern crate fern;
#[macro_use]
extern crate lazy_static;
extern crate mycelium_lib;
#[macro_use]
extern crate log;
extern crate rand;
#[macro_use]
extern crate serde;
extern crate serde_json;

use std::panic::catch_unwind;
use std::sync::mpsc::channel;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, Instant};

use mycelium_lib::prelude::*;

use crate::report::Report;

mod record;
mod report;
mod test;

use crate::test::*;

const DATADIR_STEW: &str = ".stew/bench";
lazy_static! {
    static ref PERFORMANCE: Arc<Mutex<Performance>> = Arc::new(Mutex::new(Performance::default()));
}

fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    match setup_logging(3) {
        Ok(_) => (),
        Err(e) => panic!("Failed to start logging: {:?}", e),
    }

    let mut result = vec![];
    result.push(catch_unwind(|| {
        bench_add();
    }));
    result.push(catch_unwind(|| {
        bench_get();
    }));
    result.push(catch_unwind(|| {
        bench_init();
    }));
    result.push(catch_unwind(|| {
        bench_update();
    }));
    result.push(catch_unwind(|| match bench_save_all() {
        Ok(_) => (),
        Err(e) => error!("Error: {:?}", e),
    }));
    result.push(catch_unwind(|| {
        broth_bench_add();
    }));
    result.push(catch_unwind(|| {
        broth_bench_get();
    }));
    result.push(catch_unwind(|| {
        broth_bench_search();
    }));
    result.push(catch_unwind(|| {
        match broth_bench_save_all() {
            Ok(_) => (),
            Err(e) => panic!("Benchmark test broth_bench_save_all() failed. {:?}", e),
        };
    }));

    end_perf_test();
    Ok(())
}

fn broth_bench_save_all() -> std::io::Result<()> {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let index = mycelium_lib::prelude::Index::init(Some(config))
        .expect("bench::broth_bench_save_all() db init() failed.");
    for x in 0..100 {
        let test = Test::new(format!("Bench test :index::save_all(): {}", x), 0);
        let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
        index.add("index_bench_save_all", &bin_test);
    }

    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        match index.save_all() {
            Ok(_) => (),
            Err(e) => panic!("Save failed in benchmark: {:?}", e),
        }
        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to acquire lock on PERFORMANCE.");
        perf.add_result("Index::save_all()", dur);
        tx.send(true).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
    Ok(())
}

// Do not care here if our bench is not capturing for all possible error arms.
// Any error is a panic.
#[allow(clippy::match_wild_err_arm)]
fn broth_bench_search() {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let index = mycelium_lib::prelude::Index::init(Some(config))
        .expect("bench::index_bench_search() db intit() failed.");
    let test = Test::new(String::from("Bench test :index_bench_search()"), 0);
    let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
    index
        .add_indexed("index_bench_search", &bin_test, &["test", "Test", "tEsT"])
        .expect("Failure.");
    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        let item = index.search("index_bench_search", &["test"]);
        let dur = inst.elapsed();
        if item.items.len() == 0 {
            panic!("No item found in search.")
        }
        let mut perf = perf.lock().expect("Failed to aquire lock on PERFORMANCE.");
        perf.add_result("Index::search()", dur);
        tx.send(item).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
}

fn broth_bench_add() {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let core = mycelium_lib::prelude::Db::init(Some(config)).expect("Failed to inti Stew.");
    let test = Test::new(String::from("Bench test :core_bench_add()"), 0);
    let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        core.add(bin_test.as_slice(), "core_broth_add()");
        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to acquire lock on PERFORMANCE.");
        perf.add_result("Core::add()", dur);
        tx.send(true).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
}

// Do not care here if our bench is not capturing for all possible error arms.
// Any error is a panic.
#[allow(clippy::match_wild_err_arm)]
fn broth_bench_get() {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let index = mycelium_lib::prelude::Index::init(Some(config))
        .expect("bench::index_bench_get() db init() failed.");
    let test = Test::new(String::from("Bench test :index_bench_get()"), 0);
    let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
    let id = index
        .add("index_bench_get()", bin_test.as_slice())
        .items
        .first()
        .unwrap()
        .get_id();
    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        let item = index.get("index_bench_get()", &[id]);
        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to aquire lock on PERFORMANCE.");
        perf.add_result("Index::get()", dur);
        tx.send(item).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
}

fn bench_save_all() -> std::result::Result<(), Box<dyn std::error::Error>> {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let core = mycelium_lib::prelude::Db::init(Some(config))
        .expect("bench::bench_save_all() db init() failed.");

    for x in 0..100 {
        let test = Test::new(format!("Test Item bench_save_all(): {}", x), 0);
        let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
        core.add(&bin_test, "bench_save_all");
    }

    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        match core.save_all() {
            Ok(_) => (),
            Err(e) => panic!("Save failed in benchmark: {:?}", e),
        }
        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to acquire lock on PERFORMANCE.");
        perf.add_result("Core::save_all()", dur);
        tx.send(true).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
    Ok(())
}

fn bench_add() {
    let test = Test::new(String::from("Bench test :bench_add()"), 0);
    let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let db = Db::init(Some(config)).expect("Failed to init Stew.");
    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        db.add(bin_test.as_slice(), "bench_add()");
        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to acquire lock on PERFORMANCE.");
        perf.add_result("Core::add()", dur);
        tx.send(true).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
}

// Do not care here if our bench is not capturing for all possible error arms.
// Any error is a panic.
#[allow(clippy::match_wild_err_arm)]
fn bench_get() {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let db = mycelium_lib::prelude::Db::init(Some(config))
        .expect("bench::bench_get() db init() failed.");
    let test = Test::new(String::from("Bench test :bench_add()"), 0);
    let bin_test = bincode::serialize(&test).expect("Failed to serialize simple test data...");
    let id = db
        .add(bin_test.as_slice(), "bench_get()")
        .items
        .first()
        .unwrap()
        .get_id();
    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        let item = db.get("bench_get()", id);
        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to aquire lock on PERFORMANCE.");
        perf.add_result("Core::get()", dur);
        tx.send(item).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
}

fn bench_update() {
    let config = Config::new().with_data_directory(DATADIR_STEW);
    let db = mycelium_lib::prelude::Db::init(Some(config))
        .expect("bench::bench_update() db init() failed.");
    db.add(b"bench update item", "bench_update");
    db.save_all().expect("Failed to save db.");
    let map = db
        .get_tag_hashmap("bench_update")
        .expect("Failed to unwrap() map: HashMap<u8; 16>, Node");
    let id = *map
        .keys()
        .next()
        .expect("Failed to unwrap *map.keys().next as id.");
    let (tx, rx) = channel();
    let (perf, tx) = (Arc::clone(&PERFORMANCE), tx.clone());
    thread::spawn(move || {
        let inst = Instant::now();
        let pass = match db.update_node(id, b"update bytes", "bench_update") {
            Ok(_) => true,
            Err(e) => {
                error!("Error: {:?}", e);
                false
            }
        };

        let dur = inst.elapsed();
        let mut perf = perf.lock().expect("Failed to lock on PERFORMANCE.");
        perf.add_result("Core::update()", dur);
        tx.send(pass).expect("Failed to send value over channel.");
    });
    rx.recv().unwrap();
}

fn bench_init() {
    let config = Config::new()
        .with_block_count(30)
        .with_block_size(1024)
        .with_data_directory(DATADIR_STEW);

    let (tx, rx) = channel();
    thread::spawn(move || {
        let inst = Instant::now();
        let db = match Db::init(Some(config)) {
            Ok(stew) => stew,
            Err(e) => {
                error!("Error in Core::init(config): {:?}", e);
                panic!("Error Core::init(config): {:?}", e)
            }
        };
        let dur = inst.elapsed();
        let mut perf = PERFORMANCE
            .lock()
            .expect("Failed to aquire lock on PERFORMANCE.");
        perf.add_result("Core::setup()", dur);
        tx.send(db).expect("Failed to send value over channel.");
    });

    rx.recv().unwrap();
}

fn end_perf_test() {
    //if cfg!(debug_assertions) { return; }
    let record = crate::record::fetch().update(
        &*PERFORMANCE
            .lock()
            .expect("Failed to aquire lock on PERFORMANCE"),
    );
    let report = Report::new().update(&record).write();
    record.save();

    // Failure if any functions execution time is outside standard deviation
    if report.fail {
        eprintln!("Report records beyond standard deviation for review.");
    }
    assert!(true);
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PerformanceResult {
    function_name: String,
    duration: Duration,
}

impl PerformanceResult {
    pub fn set_function_name(&mut self, name: &str) {
        self.function_name = String::from(name);
    }
    pub fn set_duraction(&mut self, duration: Duration) {
        self.duration = duration;
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Performance {
    pub test_name: String,
    pub fnc_perf_results: Vec<PerformanceResult>,
}

impl Performance {
    pub fn add_result(&mut self, fnc_name: &str, duration: Duration) {
        self.fnc_perf_results.push(PerformanceResult {
            function_name: String::from(fnc_name),
            duration,
        })
    }
}

impl Default for Performance {
    fn default() -> Performance {
        Performance {
            test_name: String::new(),
            fnc_perf_results: Vec::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn bench() {
        match crate::main() {
            Ok(_) => (),
            Err(e) => assert!(false, format!("Error benchmark::main: {:?}", e)),
        }
    }
}
