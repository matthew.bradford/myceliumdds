use super::*;
use bincode;
use rayon::prelude::*;
use uuid;

#[test]
fn test_true() {
    assert!(true)
}

#[test]
fn new_list() {
    let list = SkipList::new(epoch::default_collector().clone());
    assert!(list.is_empty());
}

#[test]
fn insert_one() {
    let list = SkipList::new(epoch::default_collector().clone());
    let guard = &epoch::pin();
    let id = *uuid::Uuid::new_v4().as_bytes();
    let value = "a duck in a pickup".as_bytes().to_vec();
    list.insert(id, value, guard);
    assert!(list.len() == 1);
}

#[test]
fn insert_10k_concurrent() {
    let list = SkipList::new(epoch::default_collector().clone());
    let mut onek: Vec<([u8; 16], Vec<u8>)> = vec![];
    for i in 0..10000 {
        let id = *uuid::Uuid::new_v4().as_bytes();
        let value = format!("a duck in a pickup: {}", i).as_bytes().to_vec();
        onek.push((id, value));
    }

    onek.par_iter().for_each(|x| {
        let guard = &epoch::pin();
        list.insert(x.0, x.1.to_vec(), guard);
    });

    let _failures: Vec<([u8; 16], Vec<u8>)> = vec![];
    onek.par_iter().for_each(|x| {
        let guard = &epoch::pin();
        let test = list.get(x.0, guard);
        if test.is_none() {
            assert!(false, "Failed to find {:?} in list.", x);
        } else {
            let test = test.unwrap();
            if test.key() != x.0 && *test.value() != x.1 {
                assert!(
                    false,
                    "Id or Value from list do not match Orig: {:?} List: {:?}",
                    x, test
                );
            }
        }
    });
    assert!(true);
}

#[test]
fn serialize_set() {
    use crate::crossbeam_skiplist::set::SkipSet;
    let set = SkipSet::new();
    for _ in 0..100 {
        let id = *uuid::Uuid::new_v4().as_bytes();
        set.insert(id);
    }

    let bin = bincode::serialize(&set).expect("Failed to serialize set");
    let set2: SkipSet<[u8; 16]> =
        bincode::deserialize(bin.as_slice()).expect("Faild to deserialize bytes");

    assert!(
        set.len() == set2.len(),
        "created and deserialized sets do not match"
    );
    for v in set {
        assert!(set2.contains(&v), "Set has item not in set2");
    }
}

#[test]
fn serialize_map() {
    let map = crate::crossbeam_skiplist::map::SkipMap::new();
    let mut onek: Vec<([u8; 16], Vec<u8>)> = vec![];
    for i in 0..100 {
        let id = *uuid::Uuid::new_v4().as_bytes();
        let value = format!("a duck in a pickup: {}", i).as_bytes().to_vec();
        onek.push((id, value));
    }

    onek.par_iter().for_each(|x| {
        map.insert(x.0, x.1.to_vec());
    });

    let json = match bincode::serialize(&map) {
        Ok(ser) => ser,
        Err(e) => {
            panic!("Error: {:?}", e);
        }
    };

    //SkipList<[u8;16], Vec<u8>
    let reb: crate::crossbeam_skiplist::map::SkipMap<[u8; 16], Vec<u8>> =
        match bincode::deserialize(&json) {
            Ok(list) => list,
            Err(e) => panic!("Error: {:?}", e),
        };

    assert!(map.len() == reb.len());

    for item in reb {
        let mitem = map.get(&item.0).unwrap();
        assert!(&item.1 == mitem.value())
    }
}
