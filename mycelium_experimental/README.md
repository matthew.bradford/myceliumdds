# Search Core

Alternative to MyceliumDDS Core this will use a concurrent skiplist instead of a structure of hash containers.

Code modified from Crossbeam projects [Experimental SkipList](https://github.com/crossbeam-rs/crossbeam/tree/master/crossbeam-skiplist). Modified version will instead use only the Uuid and byte vectors without generics for data store in Db while Index will use the generics version.

