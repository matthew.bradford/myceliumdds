#!/bin/bash

cargo build --bin mycelium --release --target x86_64-unknown-linux-musl

docker-compose stop -t 2
docker-compose rm -f

docker image build -t mycelium:0.1.0 ./

docker-compose up