use crate::{node::Node, notify::Notify, ReturnPkg};
use rayon::prelude::*;

///
/// # Summary
/// Struct used to build return information about tasks performed in the database.
///
/// * Members
///  - errors: collection of error information of summary action
///  - messages: non error message information of summary action
///  - items: results of action the database is doing
///  - schedule: items to add to the work scheduler resulting from the summary action performed.
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Summary {
    pub errors: Vec<String>,
    pub messages: Vec<String>,
    pub items: Vec<(String, [u8; 16], Option<Node>)>,
    pub schedule: Vec<Notify>,
    pub valid: bool,
}

impl Default for Summary {
    fn default() -> Summary {
        Summary {
            errors: vec![],
            items: vec![],
            messages: vec![],
            schedule: vec![],
            valid: true,
        }
    }
}

impl Summary {
    pub fn new() -> Summary {
        Summary::default()
    }

    pub fn add_error<S>(&mut self, err: S)
    where
        S: Into<String>,
    {
        self.errors.push(err.into())
    }

    ///
    /// # Append
    /// As results move from db core layers up through higher level libraries
    /// branched code paths may produce different append structs. This will
    /// append the results of Summary structs together.
    ///
    pub fn append(&mut self, summary: &mut Summary) {
        self.errors.append(&mut summary.errors);
        self.items.append(&mut summary.items);
        self.messages.append(&mut summary.messages);
        self.schedule.append(&mut summary.schedule);
    }

    pub fn push_item(&mut self, item: Option<(String, [u8; 16], Option<Node>)>) {
        match item {
            Some(i) => {
                self.items.push(i);
            }
            None => (),
        }
    }

    pub fn schedule(&mut self, ntfc: Notify) {
        self.schedule.push(ntfc)
    }

    ///
    /// # Return Package
    /// Process the command summary into a return package.
    ///
    /// * Summary
    ///   - pub errors: Vec\<String\>,
    ///   - pub messages: Vec\<String\>,
    ///   - pub response: SummaryPkg,
    ///   - pub schedule: Vec\<Notify\>,
    ///
    /// * Summary Package
    ///   - Option<Vec<[u8; 16]>>,
    ///   - Option\<SummaryList\>,
    ///
    pub fn to_return_package(self) -> ReturnPkg {
        let mut pkg = (None, None);

        // Collect the nodes into a result list of (id, bytes)
        let set = self
            .items
            .into_par_iter()
            .map(|x| {
                let mut item = None;
                if let Some(node) = x.2 {
                    item = Some(node.get_item());
                }
                (x.0, x.1, item)
            })
            .collect::<Vec<_>>();
        if set.len() > 0 {
            pkg.0 = Some(set);
        }

        // Errors/Messages about operation
        let mut emlst = vec![];
        let mut msg = self.messages;
        let mut err = self.errors;
        emlst.append(&mut msg);
        emlst.append(&mut err);
        if emlst.len() > 0 {
            pkg.1 = Some(emlst);
        }
        pkg
    }

    pub fn with_error<S>(mut self, err: S) -> Self
    where
        S: Into<String>,
    {
        self.errors.push(err.into());
        self
    }

    pub fn with_message(mut self, msg: &str) -> Self {
        self.messages.push(msg.to_string());
        self
    }

    pub fn with_result_list(mut self, mut lst: Vec<(String, [u8; 16], Option<Node>)>) -> Self {
        self.items.append(&mut lst);
        self
    }
}
