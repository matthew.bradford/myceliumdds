#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Notify {
    None,
    CreateTag(String),
    CreateNode(String, [u8; 16]),
}
