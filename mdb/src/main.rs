extern crate clap;
extern crate console;
extern crate indicatif;
extern crate mycelium_lib;
extern crate ring;
#[macro_use]
extern crate serde;
extern crate serde_json;

use std::fs::{read, OpenOptions};
use std::io::Write;
use std::path::PathBuf;
use std::process::{self};

use clap::{App, Arg};
use indicatif::{ProgressBar, ProgressStyle};
//use ring::aead::*;
//use ring::pbkdf2::*;
use mycelium_lib::prelude::tcp_call_resposne;

#[derive(Debug, Deserialize, Serialize)]
enum EType {
    // (File Bytes, File Name, Extension)
    File(Vec<u8>, String, String),
    Statement(String),
    Skip,
}

///
/// *-/ MDB \-*
///
/// Quick and dirty command line client for Mycelium. This is meant
/// primarily as a testing utility. A later version will be written
/// (if i come up better use cases outside testing data distribution
/// over nodes).
///
fn main() {
    let matches = App::new("mdb")
        .version("0.1.0")
        .author("Matthew Bradford <matthew.bradford@tutanota.com>")
        .about("Mycelium shell utility")
        .arg(
            Arg::with_name("ip")
                .required(true)
                .index(1)
                .short("i")
                .long("Ip")
                .help("IP to Mycelium node d for default(127.0.0.1): mdb -i=d ..."),
        )
        .arg(
            Arg::with_name("port")
                .required(true)
                .index(2)
                .short("p")
                .long("Port")
                .help("Port of Mycelium node d for default(34120): mdb -i=d -p=d ..."),
        )
        .arg(
            Arg::with_name("value")
                .index(3)
                .required(true)
                .help("Mycelium SQL statement (default) or file path (add file flag): mdb f ..."),
        )
        .arg(
            Arg::with_name("tag")
                .short("t")
                .index(4)
                .required(false)
                .help("Mycelium tag to store value in."),
        )
        .arg(
            Arg::with_name("phrase")
                .long("Phrase")
                .index(5)
                .required(false)
                .help("Pass phrase required to unencrypt."),
        )
        .arg(
            Arg::with_name("out_path")
                .long("Out")
                .index(6)
                .required(false)
                .help("Directory to write response to."),
        )
        .arg(
            Arg::with_name("file_flag")
                .short("f")
                .long("File")
                .help("Indicates stmt is a path"),
        )
        .arg(
            Arg::with_name("encrypt_flag")
                .short("e")
                .long("Encrypt")
                .help("Indicates value will be encrypted and stored not executed."),
        )
        .arg(
            Arg::with_name("mid_flag")
                .short("m")
                .help("flag indicating value is array of Id's [myceliumuuid, myceliumuuid]"),
        )
        .arg(Arg::with_name("save").short("s").help("Save to directory."))
        .get_matches();

    let mut port = "34120";
    if let Some(prt) = matches.value_of("port") {
        if prt != "d" {
            port = prt;
        }
    }
    let mut ip = "127.0.0.1";
    if let Some(ipc) = matches.value_of("ip") {
        if ipc != "d" {
            ip = ipc;
        }
    }
    let value = match matches.value_of("value") {
        Some(value) => value,
        None => "",
    };
    let tag = match matches.value_of("tag") {
        Some(tag) => tag,
        None => "",
    };
    let _phrase = match matches.value_of("phrase") {
        Some(phrase) => phrase,
        None => "",
    };
    let out = match matches.value_of("out_path") {
        Some(out) => out,
        None => "",
    };
    let flags = (
        matches.is_present("file_flag"),
        matches.is_present("encrypt_flag"),
        matches.is_present("mid_flag"),
        matches.is_present("save"),
    );

    match flags {
        // is a file and encrypt -- send
        (true, true, false, false) => {
            // open file -> read all bytes -> encrypt
            // require tag and phrase
            unimplemented!()
        }
        // not file and encrypt -- send
        (false, true, false, false) => {
            // turn value to bytes -> encrypt
            // require tag and phrase
            unimplemented!()
        }
        // is a file, but unencrypted -- send
        (true, false, false, false) => {
            // turn to bytes and pass to storage
            // require tag
            let path = PathBuf::from(value);
            let read = read(&path)
                .map_err(|e| {
                    println!("Failed to read file: {:?}", e);
                })
                .expect("Failed to read file.");

            let name = path
                .file_name()
                .expect("Failed to get file name.")
                .to_str()
                .expect("osstr to str fail");
            let extension = path
                .extension()
                .expect("Failed to get extension.")
                .to_str()
                .expect("osstr to str fail");
            let etype = EType::File(read, name.to_string(), extension.to_string());
            let pkg = bincode::serialize(&etype).expect("Serialize etype to bin failure.");
            let pkg_str = serde_json::to_string(&pkg).expect("Serde to json failure.");
            print_result(tcp_call_resposne(
                ip,
                port,
                format!("msql insert {} into {}", pkg_str, tag),
            ))
        }
        // not a file, not encrypted, list of ids -- retrieve
        // mdb -m d d "
        (false, false, true, save) => {
            if tag.is_empty() {
                println!("Tag required.");
                process::exit(1);
            }

            let ids: Vec<String> = match serde_json::from_str(value) {
                Ok(v) => v,
                Err(e) => {
                    println!("{:?}", e);
                    Vec::new()
                }
            };

            let mut result = Vec::new();
            for id in ids.iter() {
                let cmd = format!("msql select from {} where id is {}", tag, id);
                let call = tcp_call_resposne(ip, port, cmd).expect("Call failed.");
                let mut thing: Vec<EType> =
                    serde_json::from_str(call.as_str()).expect("Failed deserialize.");
                result.append(&mut thing);
            }
            result.iter().for_each(|line| {
                if save {
                    let mut path = PathBuf::from(out);
                    match line {
                        EType::File(b, n, e) => {
                            path.push(format!("{}.{}", n, e));
                            let mut file = OpenOptions::new()
                                .write(true)
                                .create(true)
                                .open(&path)
                                .expect("Failed to create file.");
                            match file.write_all(&b) {
                                Ok(_) => {}
                                Err(e) => {
                                    println!("Failed to write file: {:?}", e);
                                }
                            }
                        }
                        EType::Statement(s) => {
                            println!("Statement: {}", s);
                        }
                        EType::Skip => println!("Type unknown skipping."),
                    }
                } else {
                    println!("{:?}", line);
                }
            });
        }
        // not a file, result encrypted, list of ids -- retrieve
        (false, true, true, save) => {
            let mut exit_early = false;
            if save {
                if out.is_empty() {
                    println!("Out required if save flagged.");
                    exit_early = true;
                }
            }
            if exit_early {
                process::exit(1);
            }

            // currently only a single item can be requested at a time.
            let ids_pwrd: Option<[(String, String); 2]> = match serde_json::de::from_str(value) {
                Ok(v) => Some(v),
                Err(e) => {
                    println!("{:?}", e);
                    None
                }
            };

            if ids_pwrd.is_some() {
                let ids_pwrd = ids_pwrd.unwrap();
                for (id, phrase) in ids_pwrd.iter() {
                    println!("id: {} pwrd: {}", id, phrase);
                }
            }
            unimplemented!()
        }
        // not file and not encrypt (normal statement) -- retrieve
        (false, false, false, save) => {
            if save && out.is_empty() {
                eprintln!("Missing out arg.");
                process::exit(1)
            }

            // pass on to mycelium as likely statement
            // no additional requirements
            let rsp = mycelium_lib::prelude::tcp_call_resposne(
                String::from(ip),
                String::from(port),
                format!("msql {}", value.trim()),
            )
            .unwrap();
            let json: Vec<Vec<u8>> = serde_json::from_str(&rsp).expect("String to bytes failure");
            let mut e_types = Vec::new();
            for barr in json {
                e_types.push(match bincode::deserialize(&barr) {
                    Ok(t) => t,
                    Err(e) => {
                        println!("Error: {:?}", e);
                        EType::Skip
                    }
                });
            }

            if save {
                let mut path = PathBuf::from(out);
                for line in e_types {
                    match line {
                        EType::File(b, n, _e) => {
                            path.push(format!("{}", n));
                            let mut file = OpenOptions::new()
                                .write(true)
                                .create(true)
                                .open(&path)
                                .expect("Failed to create file.");
                            match file.write_all(&b) {
                                Ok(_) => println!("File written to {:?}", path),
                                Err(e) => {
                                    println!("Failed to write file: {:?}", e);
                                }
                            }
                        }
                        EType::Statement(s) => {
                            println!("Statement: {}", s);
                        }
                        EType::Skip => {
                            println!("Type not expected skipping");
                        }
                    }
                }
            }
        }
        _ => {
            let result = "Incorrect combination of flags given. Combinations: \r\n \
                          none - execute statement. \r\n \
                          -e -f value contains a filepath and should be encrypted with phrase \r\n \
                          -e unknown value should be encrypted with phrase \r\n \
                          -m value contains array of tuples (uuid, phrase) \r\n \
                          -m -s save results from value of array of tuples";
            println!("{}", result);
        }
    }
}

// Eventually we will want to format and make pretty
// results.
fn print_result(result: std::io::Result<String>) {
    match result {
        Ok(resp) => println!("{}", resp),
        Err(e) => println!("{:?}", e),
    }
}